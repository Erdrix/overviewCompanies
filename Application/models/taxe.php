<?php

class Taxe extends Model
{

    /**
     * Nom de la table
     * @var string
     */
    protected static $_table = 'taxes';

    protected static $_join = [
        'companies' => ['idCompany', 'id'],
    ];

    /**
     * Récupere la liste des stages d'après l'id d'une entreprise
     *
     * @param  string $id : Id de l'entreprise
     *
     * @return array
     */
    public static function byEntreprise($id)
    {
        return self::finds(['idCompany' => $id]);
    }
}
