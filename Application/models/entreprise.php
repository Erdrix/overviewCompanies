<?php

class Entreprise extends Model
{

    /**
     * Nom de la table
     * @var string
     */
    protected static $_table = 'companies';

    /**
     * Jointure
     * @var array
     */
    protected static $_join = [
        'cities'  => ['idCity', 'id'],
        'sectors' => ['idSector', 'id']
    ];

    /**
     * Retourne une liste des nom présent en base de donnée
     *
     * @return array
     */
    public static function names()
    {
        $id   = static::$_id;
        $data = static::finds([], [$id, 'name']);
        $temp = [];
        foreach ($data as $value) {
            $temp[$value->name] = $value->$id;
        }

        return $temp;
    }
}
