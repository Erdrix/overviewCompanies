<?php

class Stage extends Model
{

    /**
     * Nom de la table
     * @var string
     */
    protected static $_table = 'interships';

    protected static $_join = [
        'students'  => ['idStudent', 'id'],
        'companies' => ['idCompany', 'id'],
        'fields'    => ['idField', 'id']
    ];

    /**
     * Récupere la liste des stages d'après l'id d'une entreprise
     *
     * @param  string $id : Id de l'entreprise
     *
     * @return array
     */
    public static function byEntreprise($id)
    {
        return self::finds(['idCompany' => $id]);
    }

    /**
     * Retourne une liste des id de convention présent en base de donnée
     *
     * @return array
     */
    public static function convention()
    {
        $id   = static::$_id;
        $data = static::finds([], [$id, 'convention']);
        $temp = [];
        foreach ($data as $value) {
            $temp[$value->convention] = $value->$id;
        }

        return $temp;
    }
}
