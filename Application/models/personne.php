<?php

class Personne extends Model
{

    /**
     * Nom de la table
     * @var string
     */
    protected static $_table = 'students';

    /**
     * Jointure
     * @var array
     */
    protected static $_join = ['cities' => ['idCity', 'id']];

    /**
     * Retourne une liste des nom et prénom présent en base de donnée
     *
     * @return array
     */
    public static function names()
    {
        $id   = static::$_id;
        $data = static::finds([], [$id, 'name', 'firstName']);
        $temp = [];
        foreach ($data as $value) {
            $temp[$value->name . ' ' . $value->firstName] = $value->$id;
        }

        return $temp;
    }
}
