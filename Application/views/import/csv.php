<section>

	@if (!isset($data))

		<h2>Importation de fichier CSV</h2>
		<form method='post' action='?url=import/csv/read' enctype='multipart/form-data' class='forms'>
			<input type='file' name='file' id='file' style='display: inline-block'/><br>
			<label for='table'>Table d'importation</label>
			<select name='table' id='table'>
				@foreach ($tables as $table)
					<option name='{{ $table }}'> {{ $table }} </option>
				@endforeach
			</select><br>
			<input type='submit' value='Envoyez le fichier'/>
		</form>

	@else

		<h1>Importation de fichier CSV</h1>
		<form method='post' action='?url=import/csv/post' class='forms'>
			<table class="table table-bordered table-striped table-hover">
				<thead>
					<tr>
						<th>Champs</th>
						<th>Assignation</th>
					</tr>
				</thead>
				<tbody id='table_body'>
					@foreach ($tables as $field)
						<tr>
							<th scope='row'>
								{{ $field }}
							</th>
							<th>
								<select name='{{ $field }}'>
									<option value='false'> ---- </option>
									@foreach ($data as $key)
										<option value='{{ $key }}'> {{ $key }} </option>
									@endforeach
								</select>
							</th>
						</tr>
					@endforeach
					</tbody>
			</table>
			<label for='primary' title="Permet d'éviter les doublons">Champs d'identification</label>
			<select id='primary' name='primary'>
				<option name='none'> ---- </option>
				@foreach ($tables as $field)
					<option value='{{ $field }}'> {{ $field }} </option>
				@endforeach
			</select><br>
			<input type='submit' value='Valider'/>
		</form>

	@endif

</section>