<section>
    {{ Flash::get('polypro') }}
    <h2>Importation de fichier provenant de Polypro</h2>
    <form method='post' action='?url=import/polypro/read' enctype='multipart/form-data' class='forms'>
        <input type='file' name='file' id='file' style='display: inline-block'/><br><br>
        <input type='submit' value='Envoyez le fichier'/>
    </form>
</section>