<script type='text/javascript' src='js/search.js'></script>
<script type='text/javascript' src='js/hide.js'></script>

<section>
    <h2>Gestion de stage</h2>

    {{ Flash::get('errors') }}

    <form method='post' action='?url=stage/modify' class='forms' style='width: 550px'>
        {{ CSRFToken::input() }}

        {{ $form->select('stage[idStudent]', $personnes, 'Etudiant', ['id', 'name', 'firstName'], 'Ajouter un étudiant') }}

        <!-- Affichage uniquement si l'option 'nouvelle personne' utilisée -->
        <div id='hidden_people' style='display: none;'>
            {{ $form->text('personne[name]' , 'Nom') }}
            {{ $form->text('personne[firstName]' , 'Prenom') }}
            {{ $form->text('personne[mail]' , 'Mail') }}
            {{ $form->text('personne[adress]', 'Adresse') }}
        </div>

        {{ $form->select('stage[idCompany]', $entreprises, 'Entreprise', ['id', 'name'], 'Ajouter entreprise') }}

        <!-- Affichage uniquement si l'option 'nouvelle entreprise' utilisée -->
        <div id='hidden_company' style='display: none;'>
            {{ $form->text('entreprise[name]' , 'Nom d\'entreprise') }}
            {{ $form->select('idSector', $sector, 'Secteur d\'activité', ['id', 'name']) }}
            {{ $form->text('entreprise[phone]' , 'Téléphone') }}
            {{ $form->text('entreprise[mail]' , 'Mail') }}
            {{ $form->text('entreprise[adress]' , 'Adresse') }}
            {{ $form->text('entreprise[country]' , 'Pays') }}
        </div>

        {{ $form->select('stage[idField]', $filieres, 'Filiere', ['id', 'name']) }}

        {{ $form->select('stage[study]', $study, 'Année') }}

        {{ $form->text('stage[convention]' , 'Numéro de convention') }}
        {{ $form->text('stage[entitled]', 'Intitulé du stage') }}

        {{ $form->radio('stage[english]', ['0' => 'Non', '1' => 'Oui'], 'Convention en anglais') }}

        {{ $form->text('stage[startAt]', 'Début de stage') }}
        {{ $form->text('stage[endAt]' , 'Fin de stage') }}

        {{ $form->hidden('id') }}
        {{ $form->submit() }}
    </form>
</section>
