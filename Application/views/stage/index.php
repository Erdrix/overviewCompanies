<link href="css/vendor/jplist/jplist.core.min.css" rel="stylesheet" type="text/css" />
<script src="js/vendor/jplist/jplist.core.min.js"></script>
<link href="css/vendor/jplist/jplist.textbox-filter.min.css" rel="stylesheet" type="text/css" />
<script src="js/vendor/jplist/jplist.textbox-filter.min.js"></script>
<link href="css/vendor/jplist/jplist.pagination-bundle.min.css" rel="stylesheet" type="text/css" />
<script src="js/vendor/jplist/jplist.pagination-bundle.min.js"></script>
<script>
	$('document').ready(function(){
		 $('#page').jplist({
		    itemsBox: '#table_body', 
		    itemPath: '.tbl-item',
		    panelPath:'.jplist-panel'

		});
	});
</script>

<section>
    {{ Flash::get('modify') }}
    <!-- pagination control -->
    <div id = 'page'>
		<table  class='table table-bordered table-striped table-hover'>
			<thead>
				<tr>
					<th>Numéro de convention</th>
					<th>Stagiaire</th>
					<th>Entreprise</th>
					<th>Filière</th>
					<th>Dates</th>
		            @if($auth->logged())
		                <th>Action</th>
		            @endif

				</tr>
				<tr class = "jplist-panel">
					<td> 
						<input 
							data-path='.conv' 
							type='text' 
							placeholder='filtre par convention' 
							data-control-type="textbox" 
							data-control-name="conv-filter" 
							data-control-action="filter" 
						/>
					</td>
					<td>
						<input 
							data-path='.stagiaire' 
							type='text'
							placeholder='filtre par stagiaire'
							data-control-type="textbox" 
							data-control-name="stagiaire-filter" 
							data-control-action="filter"
						/>
					</td>
					<td>
						<input					
							data-path='.ent' 
							type='text'
							placeholder='filtre par ntreprise' 
							data-control-type="textbox" 
							data-control-name="ent-filter" 
							data-control-action="filter"
							value = "{{$ent}}"
						/>
					</td>
					<td>
						<input 
							data-path='.filiere'
							type='text'
							placeholder='filtre par filières'    
							data-control-type="textbox" 
							data-control-name="filiere-filter" 
							data-control-action="filter"
						/>
					</td>
					<td>
						<input 
							data-path='.date'
							type='text'
							placeholder='filtre par dates'
							data-control-type="textbox" 
							data-control-name="date-filter" 
							data-control-action="filter"       
						/>
					</td>
				</tr>
			</thead>

			<tbody id='table_body'>
		
				@require('POST', '/request')

			</tbody>
			
		</table>
		<div  class="jplist-panel box panel-top">						
								
			<div 
				class="jplist-drop-down" 
				data-control-type="items-per-page-drop-down" 
				data-control-name="paging" 
				data-control-action="paging">
							   
				<ul>
					<li><span data-number="3"> 3 per page </span></li>
					<li><span data-number="5"> 5 per page </span></li>
					<li><span data-number="10" data-default="true"> 10 per page </span></li>
					<li><span data-number="all"> view all </span></li>
				</ul>
			</div>
							
			<div 
				class="jplist-label" 
				data-type="Page {current} of {pages}" 
				data-control-type="pagination-info" 
				data-control-name="paging" 
				data-control-action="paging">
			</div>	

			<div 
				class="jplist-pagination" 
				data-control-type="pagination" 
				data-control-name="paging" 
				data-control-action="paging">
			</div>	
							
		</div>
	</div>
	
</section>