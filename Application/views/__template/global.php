<!DOCTYPE html>
<html lang='fr'>
   <head>
      <meta charset='utf-8'>
      <title>{{ isset($title) ? $title : 'Overview Company' }}</title>
      <meta name='viewport' content='width=device-width, initial-scale=1.0'>

      <!-- Css -->
      <link href='css/vendor/bootstrap/bootstrap.min.css' rel='stylesheet'>
      <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"> 
      <link href='css/global.css' rel='stylesheet'>
      <link href='css/input.css' rel='stylesheet'>
      <link href='css/font.css' rel='stylesheet'>

      <!-- Favicon -->
      <link rel='shortcut icon' href='img/favicon.ico'>

      <!-- Jquery -->
      <script src='js/vendor/jquery/jquery.min.js'></script>
      <script src='js/vendor/bootstrap/bootstrap.min.js'></script>

      <!-- Gestion du html5 pour IE6-8. -->
      <!--[if lt IE 9]>
         <script src='js/vendor/html5shiv/html5shiv.js'></script>
         <script src='js/vendor/respond/respond.min.js'></script>
      <![endif]-->

      <script>
      $(function(){
        $('.navigation_left.overable').hover(function(){

         $(this).find('.onglet').fadeOut();
        }, function(){
          $(this).find('.onglet').fadeIn();
        });
      });
      </script>
    </head>

   <body>

      <!-- Top navigation -->
      <nav class='navigation_header'>
         <a href='?url=' class='navigation_header_title'>
            OverView
            <span>Companies</span>
         </a>

         <ul class='navigation_header_link'>
            <li @if(!isset($navigation)) class='active' @endif ><a href='?url='> Accueil</a></li>
            <li @if(isset($navigation) && $navigation == 'overview') class='active' @endif><a href='?url=entreprise/overview'> Overview</a></li>
            <li @if(isset($navigation) && $navigation == 'entreprise') class='active' @endif><a href='?url=entreprise'> Entreprises</a></li>
            <li @if(isset($navigation) && $navigation == 'personne') class='active' @endif><a href='?url=personne'> Personnes</a></li>
            <li @if(isset($navigation) && $navigation == 'stage') class='active' @endif><a href='?url=stage'> Stages</a></li>
            <li @if(isset($navigation) && $navigation == 'taxe') class='active' @endif><a href='?url=taxe'> Taxe</a></li>
         </ul>

         <?php
            $__auth  = Container::get('Auth');
            $__admin = $__auth->logged();
         ?>
         <div class='navigation_header_login'>
             <div class="dropdown">
                 @if($__admin)
                 <div class="dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
                     {{ ucfirst($__auth->user()->username) }}
                     <span class="caret"></span>
                 </div>
                 @else
                    <a href='?url=user/login' style='color: white'>Connexion</a>
                 @endif

                 @if($__admin)
                     <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                         <li role="presentation"><a role="menuitem" tabindex="-1" href="?url=user/">Parametres</a></li>
                         <li class="divider"></li>
                         <li role="presentation"><a role="menuitem" tabindex="-1" href="?url=user/disconnect">Déconnexion</a></li>
                     </ul>
                 @endif
             </div>
         </div>
      </nav>

      <!-- Left side navigation -->
      <nav class="navigation_left navigation_left_add {{ $__admin ? 'overable' : '' }}">
          @if($__admin)
         <div class='onglet'>
            <span class="fa fa-plus"></span>
            Ajouter
         </div>
         <div class='navigation_left_title'>
            <span class="fa fa-plus"></span>
            Ajouter
         </div>

         <ul class='navigation_left_link'>
            <li><a href='?url=entreprise/add'><span class="fa fa-building"></span>  Entreprise</a></li>
            <li><a href='?url=personne/add'><span class="fa fa-user-plus"></span> Personne</a></li>
            <li><a href='?url=stage/add'><span class="fa fa-briefcase"></span> Stage</a></li>
            <li><a href='?url=taxe/add'><span class="fa fa-eur"></span> Taxe</a></li>
         </ul>
          @endif
      </nav>
       <nav class="navigation_left navigation_left_import {{ $__admin ? 'overable' : '' }}">
          @if($__admin)
           <div class='onglet'>
            <span class="fa fa-download"></span>  Importer
         </div>
         <div class='navigation_left_title'>
            <span class="fa fa-download"></span> Importer
         </div>

         <ul class='navigation_left_link'>
            <li><a href='?url=import/csv'><span class="fa fa-file-excel-o"></span> CSV</a></li>
            <li><a href='?url=import/polypro'><span class="fa fa-file"></span> Ipro</a></li>
         </ul>
          @endif
      </nav>
      <?php unset($__auth, $__admin); ?>

      <!-- Content -->
      <div class='page_content'>
         @template@

         <div style='font-size: 15px; text-align: center;'>
            Page générée en <span style='color: rgb(26, 78, 158)'>{{ round(microtime(TRUE) - TIME, 3) }}</span>s.
         </div>
      </div>
   </body>
</html>
