<link href="css/vendor/jplist/jplist.core.min.css" rel="stylesheet" type="text/css" />
<script src="js/vendor/jplist/jplist.core.min.js"></script>
<link href="css/vendor/jplist/jplist.textbox-filter.min.css" rel="stylesheet" type="text/css" />
<script src="js/vendor/jplist/jplist.textbox-filter.min.js"></script>
<link href="css/vendor/jplist/jplist.pagination-bundle.min.css" rel="stylesheet" type="text/css" />
<script src="js/vendor/jplist/jplist.pagination-bundle.min.js"></script>
<script>
	$('document').ready(function(){
		 $('#page').jplist({
		    itemsBox: '#table_body', 
		    itemPath: '.tbl-item',
		    panelPath:'.jplist-panel'

		});
	});
</script>
<section>
	<div id ="page">
		<table class='table table-bordered table-striped table-hover'>
			<thead>
				<tr>
					<th>Numéro de taxe</th>
					<th>Entreprise</th>
					<th>Montant</th>
					<th>Année</th>
	                @if($auth->logged())
	                    <th>Action</th>
	                @endif
				</tr>
				<tr class = "jplist-panel">
					<td>
						<input 
							data-path='.id'
							type='text'
							placeholder='filtre par taxe'
							data-control-type="textbox" 
							data-control-name="taxe-filter" 
							data-control-action="filter"       
						/>
					</td>
					<td>
						<input 
							data-path='.companies'
							type='text'
							value = "{{$ent}}"
							placeholder='filtre par entreprise'
							data-control-type="textbox" 
							data-control-name="companies-filter" 
							data-control-action="filter"       
						/>
					</td>
					<td>
						<input 
							data-path='.amount'
							type='text'
							placeholder='filtre par montant'
							data-control-type="textbox" 
							data-control-name="amount-filter" 
							data-control-action="filter"       
						/>
					</td>
					<td>
						<input 
							data-path='.year'
							type='text'
							placeholder='filtre par années'
							data-control-type="textbox" 
							data-control-name="year-filter" 
							data-control-action="filter"       
						/>
					</td>
	                @if($auth->logged())
	                    <td></td>
	                @endif
				</tr>
			</thead>

			<tbody id='table_body'>

				@require('POST', '/request')

			</tbody>
		</table>
		<div  class="jplist-panel box panel-top">						
								
			<div 
				class="jplist-drop-down" 
				data-control-type="items-per-page-drop-down" 
				data-control-name="paging" 
				data-control-action="paging">
							   
				<ul>
					<li><span data-number="3"> 3 per page </span></li>
					<li><span data-number="5"> 5 per page </span></li>
					<li><span data-number="10" data-default="true"> 10 per page </span></li>
					<li><span data-number="all"> view all </span></li>
				</ul>
			</div>
							
			<div 
				class="jplist-label" 
				data-type="Page {current} of {pages}" 
				data-control-type="pagination-info" 
				data-control-name="paging" 
				data-control-action="paging">
			</div>	

			<div 
				class="jplist-pagination" 
				data-control-type="pagination" 
				data-control-name="paging" 
				data-control-action="paging">
			</div>	
							
		</div>
	</div>
</section>