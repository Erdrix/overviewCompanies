<script type='text/javascript' src='js/hide.js'></script>

<section>
    <h2>Gestion de taxe</h2>

    {{ Flash::get('errors') }}

    <form method='post' action='?url=taxe/modify' class='forms' style='width: 550px'>
        {{ CSRFToken::input() }}

          {{ $form->select('taxe[idCompany]', $entreprises, 'Entreprise', ['id', 'name'], 'Ajouter entreprise') }}

        <!-- Affichage uniquement si l'option 'nouvelle entreprise' utilisée -->
        <div id='hidden_company' style='display: none;'>
            {{ $form->text('entreprise[name]' , 'Nom d\'entreprise') }}
            {{ $form->select('idSector', $sector, 'Secteur d\'activité', ['id', 'name']) }}
            {{ $form->text('entreprise[phone]' , 'Téléphone') }}
            {{ $form->text('entreprise[mail]' , 'Mail') }}
            {{ $form->text('entreprise[adress]' , 'Adresse') }}
            {{ $form->text('entreprise[country]' , 'Pays') }}
        </div>

        {{ $form->text('taxe[amount]' , 'Montant (en €)') }}
        {{ $form->text('taxe[year]', 'Année') }}


        {{ $form->hidden('id') }}
        {{ $form->submit() }}
    </form>
</section>
