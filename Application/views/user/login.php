<section>
    <h2>Connexion utilisateur</h2>

    {{ Flash::get('login') }}
    <form method='POST' class='forms'>
        {{ $form->text('username', 'Nom d\'utilisateur') }}
        {{ $form->password('password', 'Mot de passe') }}
        {{ $form->submit('Connexion') }}
    </form>

</section>