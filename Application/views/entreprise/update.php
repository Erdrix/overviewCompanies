<section>
    <h2>Gestion d'entreprise</h2>

    {{ Flash::get('errors') }}

    <form method='post' action='?url=entreprise/modify' class='forms'>
        {{ CSRFToken::input() }}

        {{ $form->text('name' , 'Nom entreprise') }}
        {{ $form->select('idSector', $sector, 'Secteur d\'activité', ['id', 'name']) }}
        {{ $form->text('phone' , 'Téléphone') }}
        {{ $form->text('mail' , 'Mail') }}
        {{ $form->text('adress', 'Adresse') }}
        {{ $form->text('country' , 'Pays') }}

        {{ $form->hidden('id') }}
        {{ $form->submit('Envoyer') }}
    </form>
</section>