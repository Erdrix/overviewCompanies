<!-- Css -->
<link rel="stylesheet" href="css/vendor/morris/morris.css">
<link rel="stylesheet" href="css/view.css">

<!-- Javascript -->
<script src="js/vendor/raphael/raphael-min.js"></script>
<script src="js/vendor/morris/morris.min.js"></script>

<section>
    <div id="level_0">
        <div id='fiche' class='tuile_50'>
            <h2><span>Fiche Entreprise</span></h2>

            <p> OvC</p>
            <img src="img/fiche_ent/triangle_vert_T.png" class='flecheT'>
            <img src="img/fiche_ent/triangle_vert_L.png" class='flecheL'>
        </div>

        <div id='entreprise' class='tuile_50'>
            <h2><span>{{ $entreprise->name }}</span></h2>

            <p>{{ $entreprise->id }}</p>
            <img src="img/fiche_ent/triangle_bleu_L.png" class='flecheL'>
        </div>
    </div>

    <div id="level_1">
        <div id='coord' class='tuile_50'>
            <h3>Coordonnées</h3>

            <div class='wrap'>
                <div class='gauche'>
                    <ul>
                        <li>
                            <img src="img/fiche_ent/coord/tel.png">
                            {{ $entreprise->phone }}
                        </li>
                        <li>
                            <img src="img/fiche_ent/coord/secteur.png">
                            {{ $entreprise->sectors_name }}
                        </li>
                    </ul>
                </div>

                <div class='droite'>
                    <ul>
                        <li>
                            <img src="img/fiche_ent/coord/mail.png">
                            {{ $entreprise->mail }}
                        </li>

                        <li>
                            <img src="img/fiche_ent/coord/lieu.png">
                            {{ $entreprise->adress }}<br>
							<span>{{ $entreprise->cities_name }} {{ $entreprise->cities_zip }}<br>
								{{ $entreprise->country }}</span>
                        </li>

                    </ul>
                </div>
            </div>
            <img src="img/fiche_ent/triangle_rouge_L.png" class='flecheL'>
        </div>

        <div id='info' class='tuile_50'>
            <h3>Informations</h3>

            <div class='wrap'>
                <div class='gauche'>
                    <ul>
                        <li>
                            <img src="img/fiche_ent/info/stage.png">
                            Nombre total de stage : <br>
                            <span>{{ count($data) }}</span>
                        </li>

                        <li>
                            <img src="img/fiche_ent/info/TA.png">
                            <?php end($taxes);?>
                            Taxe d'apprentissage({{key($taxes)}}) : <br>
                            <span>{{$taxes[key($taxes)]}}</span>
                            <?php reset($taxes);?>

                        </li>

                        <li>
                            <img src="img/fiche_ent/info/forum.png">
                            Participation au forum : <br>
                            <span>??</span>
                        </li>
                    </ul>
                </div>

                <div class='droite'>
                    <ul>
                        <li>
                            <img src="img/fiche_ent/info/stage.png">
                            [champs à définir]<br>
                            <span>??</span>
                        </li>

                        <li>
                            <img src="img/fiche_ent/info/stage.png">
                            [champs à définir]<br>
                            <span>??</span>
                        </li>
                    </ul>
                </div>
            </div>
            <img src="img/fiche_ent/triangle_orange_L.png" class='flecheL'>
        </div>
    </div>

    <div id="level_2">
        <div id='stageT' class='tuile_25'>
            <h3>Stages</h3>

            <div class='wrap'>
                <ul>
                    <li>
                        <img src="img/fiche_ent/stage/stage.png">
                        Stages moyen par an : <br>
                        <span>{{ $average }}</span>
                    </li>

                    <li>
                        <img src="img/fiche_ent/stage/stage.png">
                        Filières concernées : <br>

                        <div class='gauche'>
                            <ul>
                                @foreach ($fili as $key)
                                <li>{{ $key }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
            <img src="img/fiche_ent/triangle_gris_T.png" class='flecheT'>
            <img src="img/fiche_ent/triangle_gris_L.png" class='flecheL'>
        </div>

        <div id='searchT' class='tuile_25'>
            <h3>Statistiques</h3>

            <div class='wrap'>
                <strong>Année :</strong>
                <select id='year'>
                    @foreach ($annees as $key)
                    <option value='{{ $key }}'>{{ $key }}</option>
                    @endforeach
                </select><br>

                <strong>De </strong>
                <select id='from'>
                    <?php
                    $select = min($date);
                    foreach ($date as $key) {
                        echo '<option ' . ($key == $select ? 'selected ' : '') . 'value=\'' . $key . '\'>' . $key .
                             '</option>';
                    }
                    ?>
                </select><br>

                <strong> a </strong>
                <select id='to'>
                    <?php
                    $select = max($date);
                    foreach ($date as $key) {
                        echo '<option ' . ($key == $select ? 'selected ' : '') . 'value=\'' . $key . '\'>' . $key .
                             '</option>';
                    }
                    ?>
                </select>
            </div>

            <h3 style="position: absolute; top: 60%;">Consulter les stages</h3>

            <a href="?url=stage/entreprise/{{$entreprise->name}}" style = "color : white;"><div class='consulte'>
                Consulter
            </div></a>
        </div>

        <div id='diagrammeT' class='tuile_25'>
            <h3>Stages par année</h3>

            <div id="lines" class="wrap" style="height: 60%; width: 90%; float: left"></div>
            <img src="img/fiche_ent/triangle_gris_T.png" class='flecheT'>
            <img src="img/fiche_ent/triangle_gris_L.png" class='flecheL'>
        </div>

        <div id='donutsT' class='tuile_25'>
            <h3>Stages par filière</h3>

            <div id="donuts" class="wrap" style="height: 60%; width: 60%; float: left"></div>
        </div>
    </div>

    <div id="level_3">
        <div id="TaxeT" class = "tuile_50">
            <h3> Taxe d'apprentissage</h3>
             <div class='wrap'>
                <strong>De </strong>
                <select id='from_2'>
                    <?php

                    $select = key($taxes);
                    foreach ($taxes as $key=> $value) {
                        echo '<option ' . ($key == $select ? 'selected ' : '') . 'value=\'' . $key . '\'>' . $key .
                             '</option>';
                    }
                    ?>
                </select><br> 

                <strong> a </strong>
                <select id='to_2'>
                    <?php
                    end($taxes);
                    $select = key($taxes);
                    foreach ($taxes as $key => $value) {
                        echo '<option ' . ($key == $select ? 'selected ' : '') . 'value=\'' . $key . '\'>' . $key .
                             '</option>';
                    }
                    ?>
                </select>

            </div>
            
            <a href="?url=taxe/entreprise/{{$entreprise->name}}" style = "color : white;"><div class='consulte'>Consulter</div></a>
            <div id="bars" class="wrap" style="height: 200px; width: 400px; float: left"></div>
            <img src="img/fiche_ent/triangle_bleu2_T.png" class='flecheT'>
        </div>
        <div id="participationT" class = "tuile_50">
            <h3> Participation à la vie de l'école</h3>
             <div class='wrap'>
                <strong>De </strong>
                <select id='from_3'>
                    <?php

                    $select = key($taxes);
                    foreach ($taxes as $key=> $value) {
                        echo '<option ' . ($key == $select ? 'selected ' : '') . 'value=\'' . $key . '\'>' . $key .
                             '</option>';
                    }
                    ?>
                </select><br> 

                <strong> a </strong>
                <select id='to_3'>
                    <?php
                    end($taxes);
                    $select = key($taxes);
                    foreach ($taxes as $key => $value) {
                        echo '<option ' . ($key == $select ? 'selected ' : '') . 'value=\'' . $key . '\'>' . $key .
                             '</option>';
                    }
                    ?>
                </select> 
            </div>
            <div class='consulte'>Consulter</div>
            <div id="bars2" class="wrap" style="height: 200px; width: 400px; float: left"></div>

        </div>
    </div>
</section>



<script type='text/javascript'>

    // Lecture des données de stage
    function readStage(data, year, debut, fin, color) {
        var l = data.length;
        var count = [];
        var years = {};
        var types = {};

        for (var i = 0; i < l; i++) {
            if (year != 'All' && year != data[i][2])
                continue;
            if (data[i][1] < debut || data[i][1] > fin)
                continue;

            if (count[data[i][0]] == null)
                count[data[i][0]] = 1;
            else
                count[data[i][0]]++;

            if (types[data[i][0]] == null)
                types[data[i][0]] = 1;

            if (years[data[i][1]] == null)
                years[data[i][1]] = {};
            if (years[data[i][1]][data[i][0]] == null)
                years[data[i][1]][data[i][0]] = 1;
            else
                years[data[i][1]][data[i][0]]++;
        }

        for (var y in years)
            for (var t in types)
                if (years[y][t] == null)
                    years[y][t] = 0;

        var result = [];
        var colors = [];
        for (var key in count) {
            result.push({label: key, value: count[key]});
            colors.push(color[key]);
        }

        var lines = [];
        var colors2 = [];
        var temp;
        for (var key in years) {
            temp = {};
            temp['y'] = key;
            for (var t in years[key])
                temp[t] = years[key][t];

            lines.push(temp);
        }

        temp = [];
        for (var t in types)
            temp.push(t);

        return [result, colors, lines, temp, colors2];
    }

    // Lecture des données de taxes
    function readTaxe(taxes, debut, fin) {
        var temp = [];
        for(var t in taxes) {
            if(taxes[t][0] < debut || taxes[t][0] > fin)
                continue;

            temp.push({y: taxes[t][0], a: taxes[t][1]});
        }

        return temp;
    }

    // Création des données brute
    var data = [
    @foreach($data as $key => $value)
    ['{{ $value[0] }}', '{{ $value[1] }}', '{{ $value[2] }}'],
    @endforeach
    ];

    // Création des codes couleurs
    var color = [];
    @foreach($colors as $key => $value)
    color['{{ $key }}'] = '{{ $value }}';
    @endforeach

    var taxes = [
        @foreach($taxes as $key => $value)
        ['{{ $key }}', '{{ $value }}'],
        @endforeach
    ];

    // Création du graph Donut
    var temp = readStage(data, $('#year').val(), $('#from').val(), $('#to').val(), color);
    var donuts = new Morris.Donut({
        element: 'donuts',
        data: temp[0],
        colors: temp[1]
    });

    // Création du graph Line
    var lines = new Morris.Line({
        element: 'lines',
        data: temp[2],
        xkey: 'y',
        ykeys: temp[3],
        labels: temp[3],
        hideHover: true,
        xLabels: 'year',
        lineColors: temp[1]
    });

    // Création du graph bar taxe
    var bars = new Morris.Bar({
        element: 'bars',
        data: readTaxe(taxes, $('#from_2').val(), $('#to_2').val()),
        xkey: 'y',
        ykeys: ['a'],
        labels: ['montant'],
        pointStrokeColors: ["#000"],
        hideHover: true,
        barColors: ['#201b1b'],
        barOpacity:0.5,
    });

    // Changement d'année
    $('#year, #from, #to').change(function () {
        if ($('#from').val() > $('#to').val())
            return;

        var temp = readStage(data, $('#year').val(), $('#from').val(), $('#to').val(), color);
        donuts.options.colors = temp[1];
        donuts.setData(temp[0]);

        lines.options.labels = temp[3];
        lines.options.ykeys = temp[3];
        lines.options.lineColors = temp[1];
        lines.setData(temp[2]);
    });

     // Changement de date taxe.
    $('#from_2, #to_2').change(function () {
        if ($('#from_2').val() > $('#to_2').val())
            return;

        var temp_2 = readTaxe(taxes, $('#from_2').val(), $('#to_2').val());

        bars.options.labels = ['montant'];
        bars.options.ykeys = ['a'];
        bars.setData(temp_2);
    });

</script>