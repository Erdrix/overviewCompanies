<link href="css/vendor/jplist/jplist.core.min.css" rel="stylesheet" type="text/css" />
<script src="js/vendor/jplist/jplist.core.min.js"></script>
<link href="css/vendor/jplist/jplist.textbox-filter.min.css" rel="stylesheet" type="text/css" />
<script src="js/vendor/jplist/jplist.textbox-filter.min.js"></script>
<link href="css/vendor/jplist/jplist.pagination-bundle.min.css" rel="stylesheet" type="text/css" />
<script src="js/vendor/jplist/jplist.pagination-bundle.min.js"></script>
<script>
	$('document').ready(function(){
		 $('#page').jplist({
		    itemsBox: '#table_body', 
		    itemPath: '.tbl-item',
		    panelPath:'.jplist-panel'

		});
	});
</script>
<section>
    {{ Flash::get('modify') }}
    <div id = 'page'>
		<table class="table table-bordered table-striped table-hover">
			<thead>
				<tr>
					<th>Nom</th>
					<th>Secteur</th>
					<th>Téléphone</th>
					<th>Adresse</th>
					<th>Pays</th>
	                @if($admin)
					    <th>Action</th>
	                @endif
				</tr>
				<tr class = "jplist-panel">
					<th>
						<input 
							data-path='.nameEnt' 
							type='text' 
							placeholder='filtre par nom' 
							data-control-type="textbox" 
							data-control-name="name-filter" 
							data-control-action="filter"    
						/>
					</th>
					<td>
					<input 
							data-path='.sectorEnt' 
							type='text' 
							placeholder='filtre par secteur' 
							data-control-type="textbox" 
							data-control-name="sector-filter" 
							data-control-action="filter"   
					/>
					</td>
					<td>
						<input 
							data-path='.phoneEnt' 
							type='text' 
							placeholder='filtre par téléphone' 
							data-control-type="textbox" 
							data-control-name="phone-filter" 
							data-control-action="filter"
						/>
					</td>
					<td>
						<input
							data-path='.citiesEnt' 
							type='text' 
							placeholder='filtre par adresse' 
							data-control-type="textbox" 
							data-control-name="cities-filter" 
							data-control-action="filter"
						</td>
					<td>
						<input 
							data-path='.countryEnt' 
							type='text' 
							placeholder='filtre par pays' 
							data-control-type="textbox" 
							data-control-name="country-filter" 
							data-control-action="filter"      
						/>
					</td>
	                @if($admin)
					    <td></td>
	                @endif
				</tr>
			</thead>

			<tbody id='table_body'>

				@require('POST', '/request')

			</tbody>
		</table>
		<div  class="jplist-panel box panel-top">						
								
			<div 
				class="jplist-drop-down" 
				data-control-type="items-per-page-drop-down" 
				data-control-name="paging" 
				data-control-action="paging">
							   
				<ul>
					<li><span data-number="3"> 3 per page </span></li>
					<li><span data-number="5"> 5 per page </span></li>
					<li><span data-number="10" data-default="true"> 10 per page </span></li>
					<li><span data-number="all"> view all </span></li>
				</ul>
			</div>
							
			<div 
				class="jplist-label" 
				data-type="Page {current} of {pages}" 
				data-control-type="pagination-info" 
				data-control-name="paging" 
				data-control-action="paging">
			</div>	

			<div 
				class="jplist-pagination" 
				data-control-type="pagination" 
				data-control-name="paging" 
				data-control-action="paging">
			</div>	
							
		</div>0
	</div>
</section>