<section>
    <h2>Gestion des personnes</h2>

    {{ Flash::get('errors') }}

    <form method='post' action='?url=personne/modify' class='forms'>
        {{ CSRFToken::input() }}

        {{ $form->text('name' , 'Nom') }}
        {{ $form->text('firstName' , 'Prénom') }}
        {{ $form->text('mail' , 'Mail') }}
        {{ $form->text('adress', 'Adresse') }}

        {{ $form->hidden('id') }}
        {{ $form->submit('Envoyer') }}
    </form>
</section>