<link href="css/vendor/jplist/jplist.core.min.css" rel="stylesheet" type="text/css" />
<script src="js/vendor/jplist/jplist.core.min.js"></script>
<link href="css/vendor/jplist/jplist.textbox-filter.min.css" rel="stylesheet" type="text/css" />
<script src="js/vendor/jplist/jplist.textbox-filter.min.js"></script>
<link href="css/vendor/jplist/jplist.pagination-bundle.min.css" rel="stylesheet" type="text/css" />
<script src="js/vendor/jplist/jplist.pagination-bundle.min.js"></script>
<script>
	$('document').ready(function(){
		 $('#page').jplist({
		    itemsBox: '#table_body', 
		    itemPath: '.tbl-item',
		    panelPath:'.jplist-panel'

		});
	});
</script>
<section>
	<div id ='page'>
		<table class="table table-bordered table-striped table-hover">
			<thead>
				<tr>
					<th>Personne</th>
					<th>Mail</th>
					<th>Adresse</th>
					<th>Ville</th>
					<th>Code Postal</th>
	                @if($auth->logged())
					    <th>Action</th>
	                @endif
				</tr>
				<tr class = "jplist-panel">
					<th>
						<input 
							data-path='.people' 
							type='text' 
							placeholder='filtre par personne' 
							data-control-type="textbox" 
							data-control-name="people-filter" 
							data-control-action="filter"    
						/>
					</th>
					<th>
						<input 
							data-path='.mail' 
							type='text' 
							placeholder='filtre par e-mail' 
							data-control-type="textbox" 
							data-control-name="mail-filter" 
							data-control-action="filter"    
						/>
					</th>
					<th>
						<input 
							data-path='.adress' 
							type='text' 
							placeholder='filtre par adresse' 
							data-control-type="textbox" 
							data-control-name="adress-filter" 
							data-control-action="filter"    
						/>
					</th>
					<th>
						<input 
							data-path='.cities' 
							type='text' 
							placeholder='filtre par ville' 
							data-control-type="textbox" 
							data-control-name="cities-filter" 
							data-control-action="filter"    
						/>
					</th>
					<th>
						<input 
							data-path='.zip' 
							type='text' 
							placeholder='filtre par code postal' 
							data-control-type="textbox" 
							data-control-name="zip-filter" 
							data-control-action="filter"    
						/>
					</th>
	                @if($auth->logged())
	                    <td></td>
	                @endif
				</tr>
			</thead>

			<tbody id='table_body'>

				@require('POST', '/request')

			</tbody>
		</table>
		<div  class="jplist-panel box panel-top">											
			<div 
				class="jplist-drop-down" 
				data-control-type="items-per-page-drop-down" 
				data-control-name="paging" 
				data-control-action="paging">
							   
				<ul>
					<li><span data-number="3"> 3 per page </span></li>
					<li><span data-number="5"> 5 per page </span></li>
					<li><span data-number="10" data-default="true"> 10 per page </span></li>
					<li><span data-number="all"> view all </span></li>
				</ul>
			</div>
							
			<div 
				class="jplist-label" 
				data-type="Page {current} of {pages}" 
				data-control-type="pagination-info" 
				data-control-name="paging" 
				data-control-action="paging">
			</div>	

			<div 
				class="jplist-pagination" 
				data-control-type="pagination" 
				data-control-name="paging" 
				data-control-action="paging">
			</div>				
		</div>
	</div>
</section>