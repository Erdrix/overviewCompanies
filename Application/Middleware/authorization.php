<?php

class authorization implements Middleware
{

    //////////////////////////
    // Arguments
    /////////

    /**
     * Authentification
     * @var Auth
     */
    private $auth;

    //////////////////////////
    // Constructeur
    /////////

    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    //////////////////////////
    // Handler
    /////////

    /**
     * Gestion de la requête
     *
     * @param Request $request : Informations sur la requête
     * @param Closure $next    : Prochaine closure
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (!$this->auth->logged()) {
            Flash::set('login', 'Vous devez vous connecter pour acceder aux pages !', 'danger');

            return Redirect::make('user/login');
        }

        return $next($request);
    }
}