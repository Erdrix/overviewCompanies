<?php

/**
 * Configuration du router
 * @template : Template à utiliser lors des rendus
 */
Router::config([
    'template'   => 'global',
    'middleware' => ['authorization'],
]);

/**
 * Interface d'upload de csv Polypro
 * @route : get:import/polypro
 */
Router::respond('GET', '/polypro', function () {
    return View::make('import.polypro');
});

/**
 * Interface de choix de fichier CSV
 * @route : get:import/csv
 */
Router::respond('GET', '/csv', function () {
    $params = [
        'title'  => 'Importation de CSV',
        'tables' => ['companies', 'students', 'internships', 'taxes'],
    ];

    return View::make('import.csv', $params);
});

/**
 * Lecture du fichier polypro
 * @route : post:import/polypro/read
 */
Router::respond('POST', '/polypro/read', function () {
    if (empty($_FILES['file'])) {
        Flash::set('polypro', 'Veuillez envoyer un fichier...', 'danger');

        return Redirect::make('import/polypro');
    }

    // Verification de l'extension
    $extension = strrchr($_FILES['file']['name'], '.');
    if ('.csv' != $extension) {
        Flash::set('polypro', 'Veuillez envoyer un fichier de type CSV', 'danger');

        return Redirect::make('import/polypro');
    }

    // Ouverture du fichier temporaire
    if (!($handle = fopen($_FILES['file']['tmp_name'], 'r'))) {
        Flash::set('polypro', 'Erreur : Impossible d\'ouvrir le fichier...', 'danger');

        return Redirect::make('import/polypro');
    }

    $students   = Personne::names();
    $stages     = Stage::convention();
    $entreprise = Entreprise::names();

    // Enleve les en-tête
    fgetcsv($handle, 1024, ',');

    // Lecture du fichier csv
    $total  = 0;
    $nb_per = 0;
    $nb_ent = 0;
    while ($data = array_map('utf8_encode', fgetcsv($handle, 1024, ','))) {
        // Si le stage existe déja
        if (isset($stages[$data[0]])) {
            continue;
        }

        // Si la personne n'existe pas encore
        if (!isset($students[$data[1]])) {
            $name               = explode(' ', $data[1]);
            $students[$data[1]] = Personne::create([
                'name'      => strtoupper($name[0]),
                'firstName' => ucwords($name[1]),
                'idCity'    => 1
            ]);
            $nb_per++;
        }

        $ent = trim(ucwords(strtolower(explode('-', $data[5])[0])));
        if (!isset($entreprise[$ent])) {
            $entreprise[$ent] = Entreprise::create([
                'name'     => $ent,
                'idCity'   => 1,
                'idSector' => 1
            ]);
            $nb_ent++;
        }

        $year = explode('année ', $data[2]);
        $year = isset($year[1]) ? $year[1] : false;

        // Création du stage
        Stage::create([
            'idCompany'  => $entreprise[$ent],
            'idStudent'  => $students[$data[1]],
            'study'      => $year !== false ? ($year . 'A') : '?',
            'convention' => $data[0],
            'english'    => $data[3] === 'NON' ? 0 : 1,
            'startAt'    => strftime("%Y-%m-%d", strtotime(str_replace('/', '-', $data[21]))),
            'endAt'      => strftime("%Y-%m-%d", strtotime(str_replace('/', '-', $data[22])))
        ]);

        $total++;
    }

    // Fermeture du fichier
    fclose($handle);

    Flash::set('polypro',
        'Importation de ' . $total . ' stage(s), ' . $nb_per . ' personne(s) et ' . $nb_ent . ' entreprise(s)',
        'success');

    return Redirect::make('import/polypro');
});

/**
 * Interface de lecture du fichier CSV
 * @route : post:import/csv/read
 */
Router::respond('POST', '/csv/read', function () {
    if (empty($_FILES['file'])) {
        return Redirect::make();
    }

    // Verification de l'extension
    $extension = strrchr($_FILES['file']['name'], '.');
    if ('.csv' != $extension) {
        return Redirect::make();
    }

    // Extraction du nom des tables
    if ($handle = fopen($_FILES['file']['tmp_name'], 'r')) {
        $data = array_map('utf8_encode', fgetcsv($handle, 1024, ','));
        fclose($handle);
    } else {
        return Redirect::make();
    }

    // Mise en session du nom de la table à utiliser
    $_SESSION['import_table'] = $_POST['table'];
    $_SESSION['import_file']  = getTemporary('files') . hash('md2', $_SERVER['REMOTE_ADDR']);

    // Sauvegarde du fichier
    move_uploaded_file($_FILES['file']['tmp_name'], $_SESSION['import_file']);

    // Récupération du nom des champs dans la base de donnée
    $query  = Container::get('Database')->describ($_POST['table']);
    $tables = [];
    foreach ($query as $key => $value) {
        $tables[] = $value['Field'];
    }
    unset($query);

    // Envois des parametres
    $params = [
        'title'  => 'Importation de CSV',
        'data'   => $data,
        'tables' => $tables,
    ];

    return View::make('import.csv', $params);
});

/**
 * Ajouts des elements dans la base de donnée CSV
 * @route : post:import/csv/post
 */
Router::respond('POST', '/csv/post', function () {
    if (empty($_POST['primary']) || !isset($_SESSION['import_table'])) {
        return Redirect::make();
    }

    // Récupération du champs de redondance
    $primary = empty($_POST['primary']) ? false : $_POST['primary'];
    unset($_POST['primary']);

    // Création de l'instance
    $database = Container::get('Database');

    // Récupération de la liste des doublons
    $doublon = $database->search($_SESSION['import_table'], [], $primary);

    // Ouverture du fichier temporaire
    if (!($handle = fopen($_SESSION['import_file'], 'r'))) {
        return Redirect::make();
    }

    // Création du tableau d'association
    $data = array_map('utf8_encode', fgetcsv($handle, 1024, ','));
    $link = [];
    foreach ($data as $data_key => $data_value) {
        foreach ($_POST as $post_key => $post_value) {
            if ($post_value == $data_value) {
                $link[$post_key] = $data_key;
            }
        }
    }

    // Si le champs d'identification des doublons n'est pas valide
    if (!isset($link[$primary])) {
        return Redirect::make();
    }

    // Lecture du fichier csv
    $temp = [];
    while ($data = fgetcsv($handle, 1024, ',')) {
        foreach ($link as $key => $value) {
            $temp[$key] = $data[$value];
        }

        if (!in_array($link[$primary], $doublon)) {
            $database->insert($_SESSION['import_table'], $temp);
        }
    }

    // Fermeture et suppression du fichier
    fclose($handle);
    unlink($_SESSION['import_file']);

    // Suppresion des données en session
    unset($_SESSION['import_table']);
    unset($_SESSION['import_file']);

    return Redirect::make('import/csv');
});
