<?php

/**
 * Configuration du router
 * @template  : Template à utiliser lors des rendus
 * @variables : Variable pré-définis pour les vues
 */
Router::config([
    'variables'  => ['navigation' => 'stage', 'study' => ['3A', '4A', '5A']],
    'template'   => 'global',
    'middleware' => ['authorization'],
]);

/**
 * Interface de listing des stages
 * @route : get:stage/
 */
Router::respond('GET', '/', function (Auth $auth) {
    return View::make('stage.index', ['title' => 'Liste des stages', 'auth' => $auth ]);
});

/**
 * Interface de listing des stages
 * @route : get:stage/
 */
Router::respond('GET', '/entreprise/{ent}', function (Auth $auth, $ent) {
    return View::make('stage.index', ['title' => 'Liste des stages', 'auth' => $auth, 'ent' => $ent]);
});

/**
 * Interface de création d'un stage
 * @route : get:stage/add
 */
Router::respond('GET', '/add', function () {
    $entreprises = Entreprise::identifiants(['name']);
    $personnes   = Personne::identifiants(['name', 'firstName']);
    $sector      = Sector::identifiants(['name']);
    $filieres    = Filiere::identifiants(['name']);

    $params = [
        'title'       => 'Création de stage',
        'entreprises' => $entreprises,
        'personnes'   => $personnes,
        'filieres'    => $filieres,
        'sector'      => $sector,
        'form'        => new FormBuilder(),
    ];

    return View::make('stage.update', $params);
});

/**
 * Interface de mise à jour d'un stage
 * @route : get:stage/modify
 */
Router::respond('GET', '/modify/{id}', function ($id) {
    $data['id']    = $id;
    $data['stage'] = Stage::find($id);
    $entreprises   = Entreprise::identifiants(['name']);
    $personnes     = Personne::identifiants(['name', 'firstName']);
    $sector        = Sector::identifiants(['name']);
    $filieres      = Filiere::identifiants(['name']);

    $params = [
        'title'       => 'Modification du stage ' . $data['stage']->convention,
        'entreprises' => $entreprises,
        'personnes'   => $personnes,
        'filieres'    => $filieres,
        'sector'      => $sector,
        'form'        => new FormBuilder($data),
    ];

    return View::make('stage.update', $params);
});

/**
 * Interface de suppression d'un stage
 * @root : get:stage/askdelete
 */
Router::respond('GET', '/askdelete/{id}', function ($id) {
    $stage  = Stage::find($id);
    $params = [
        'title' => 'Suppression du stage ' . $stage->convention,
        'stage' => $stage,
    ];

    return View::make('stage.askdelete', $params);
});

/**
 * Suppression d'un stage
 * @root : post:stage/delete
 */
Router::respond('GET', '/delete/{id}', function ($id) {
    $valide = Stage::delete($id) !== false;

    if (!$valide) {
        Flash::set('modify', 'Impossible de supprimer le stage numéro ' . $id, 'danger');
    } else {
        Flash::set('modify', 'Le stage numéro ' . $id . ' vient d\'être supprimé', 'success');
    }

    return Redirect::make('stage');
});

/**
 * Récupération de la liste des stages
 * @route : post:stage/request
 */
Router::respond('POST', '/request', function (Auth $auth) {
    $data = false;
    if (isset($_POST['data']) && '*' != $_POST['data']) {
        $link = [
            's' => 'ovc_students.name',
            'e' => 'ovc_companies.name',
            'f' => 'ovc_fields.name',
            'd' => 'startAt',
        ];

        $temp = explode('@', $_POST['data']);
        $data = [];
        foreach ($temp as $part) {
            $part = trim($part);
            $temp = explode(':', $part);
            if (isset($temp[1]) && isset($link[$temp[0]])) {
                $data[$link[$temp[0]]] = '%' . $temp[1] . '%';
            } else {
                $data['convention'] = '%' . $part . '%';
            }
        }
    }

    $stages = $data ? Stage::finds($data) : Stage::all();
    $stages = array_escape($stages);
    $result = '';
    $admin  = $auth->logged();

    foreach ($stages as $key => $value) {
        $result .= '<tr class = "tbl-item">';
        $result .= '<td class = "conv" scope="row">' . $value->convention . '</th>';
        $result .= '<td class = "stagiaire">' . (($value->idStudent !== null) ? $value->students_name . ' ' . $value->students_firstName : '?') . '</td>';
        $result .= '<td class = "ent" >' . (($value->idCompany !== null) ? $value->companies_name : '?') . '</td>';
        $result .= '<td class = "filiere">' . $value->fields_name . '</td>';
        $result .= '<td class = "date">De ' . $value->startAt . '<br>A ' . $value->endAt . '</td>';
        if ($admin) {
            $result .= '<td>';
            $result .= ' <a href="?url=stage/modify/' . $value->id . '/" class="btn btn-primary">Modifier</a>';
            $result .= ' <a href="?url=stage/askdelete/' . $value->id . '/" class="btn btn-danger">Supprimer</a>';
            $result .= '</td>';
        }
        $result .= '</tr>';
    }

    return $result;
});

/**
 * Mise à jour d'une entreprise
 * @route : post:entreprise/update
 */
Router::respond('POST', '/modify', function () {
    $id     = isset($_POST['id']) ? $_POST['id'] : false;
    $stage  = $_POST['stage'];
    $valide = false;
    unset($_POST['id']);

    // Création d'un utilisateur
    if ($stage['idStudent'] === "-1" && isset($_POST['personne'])) {
        $stage['idStudent'] = Personne::create($_POST['personne']);
    }

    // Création d'une entreprise
    if ($stage['idCompany'] === "-1" && isset($_POST['entreprise'])) {
        $stage['idCompany'] = Entreprise::create($_POST['entreprise']);
    }

    // Création ou modification de l'entreprise
    if ($stage['idStudent'] !== false && $stage['idCompany'] !== false) {
        if ($id !== false) {
            $valide = Stage::update($id, $stage) !== false;
        } else {
            $valide = Stage::create($stage) !== false;
        }
    }

    if ($valide) {
        Flash::set('modify', "L'opération s'est déroulée avec succès", 'success');
    } else {
        Flash::set('modify', "Une erreur est survenue", 'danger');
    }

    return Redirect::make('stage');
});