<?php

/**
 * Configuration du router
 * @template  : Template à utiliser lors des rendus
 * @variables : Variable pré-définis pour les vues
 */
Router::config([
    'variables'  => ['navigation' => 'personne'],
    'template'   => 'global',
    'middleware' => ['authorization'],
]);

/**
 * Interface de listing des personnes
 * @route : get:personne/
 */
Router::respond('GET', '/', function (Auth $auth) {
    return View::make('personne.index', ['title' => 'Recherche de personnes', 'auth' => $auth]);
});

/**
 * Interface de création de personne
 * @route : get:personne/add
 */
Router::respond('GET', '/add', function () {
    return View::make('personne.update', ['title' => 'Création d`une personne', 'form' => new FormBuilder()]);
});

/**
 * Interface de mise à jour d'une personne
 * @route : get:personne/modify
 */
Router::respond('GET', '/modify/{id}', function ($id) {
    $personne = Personne::find($id);
    $params   = [
        'title' => 'Modification de ' . $personne->name . ' ' . $personne->firstName,
        'form'  => new FormBuilder($personne),
    ];

    return View::make('personne.update', $params);
});

/**
 * Interface de suppression d'une personne
 * @root : get:personne/askdelete
 */
Router::respond('GET', '/askdelete/{id}', function ($id) {
    $personne = Personne::find($id);
    $params   = [
        'title'    => 'Suppression de ' . $personne->name,
        'personne' => $personne,
    ];

    return View::make('personne.askdelete', $params);
});

/**
 * Suppression d'une personne
 * @root : post:personne/delete
 */
Router::respond('GET', '/delete/{id}', function ($id) {
    $valide = Personne::delete($id) !== false;

    if (!$valide) {
        Flash::set('modify', 'Impossible de supprimer la personne numéro ' . $id, 'danger');
    } else {
        Flash::set('modify', 'La personne numéro ' . $id . ' vient d\'être supprimée', 'success');
    }

    return Redirect::make('personne');
});

/**
 * Récupération de la liste des personne
 * @route : post:personne/request
 */
Router::respond('POST', '/request', function (Auth $auth) {
    $data = false;
    if (isset($_POST['data']) && '*' !== $_POST['data']) {
        $link = [
            'm' => 'mail',
            'a' => 'adress',
            'v' => 'ovc_cities.name',
            'c' => 'ovc_cities.zip',
        ];

        $temp = explode('@', $_POST['data']);
        $data = [];
        foreach ($temp as $part) {
            $part = trim($part);
            $temp = explode(':', $part);
            if (isset($temp[1]) && isset($link[$temp[0]])) {
                $data[$link[$temp[0]]] = '%' . $temp[1] . '%';
            } else {
                $data['name'] = '%' . $part . '%';
            }

        }
    }

    $data   = $data ? Personne::finds($data) : Personne::all(50);
    $data   = array_escape($data);
    $result = '';
    $admin  = $auth->logged();
    foreach ($data as $personne) {
        $result .= '<tr class = "tbl-item">';
        $result .= '<th scope="row" class ="people">' . $personne->name . ' ' . $personne->firstName . '</th>';
        $result .= '<td class = "mail">' . $personne->mail . '</td>';
        $result .= '<td class = "adress">' . $personne->adress . '</td>';
        $result .= '<td class = "cities">' . $personne->cities_name . '</td>';
        $result .= '<td class = "zip">' . $personne->cities_zip . '</td>';
        if ($admin) {
            $result .= '<td>';
            $result .= ' <a href="?url=personne/modify/' . $personne->id . '/" class="btn btn-primary">Modifier</a>';
            $result .= ' <a href="?url=personne/askdelete/' . $personne->id . '/" class="btn btn-danger">Supprimer</a>';
            $result .= '</td>';
        }
        $result .= '</tr>';
    }

    return $result;
});

/**
 * Mise à jour d'une personne
 * @route : post:personne/update
 */
Router::respond('POST', '/modify', function (Validator $validator) {
    $valide = false;
    $id     = isset($_POST['id']) ? $_POST['id'] : false;
    unset($_POST['id']);

    $validator->check($_POST, [
        'name'      => ['required' => true],
        'firstName' => ['required' => true],
        'mail'      => ['default' => '?'],
        'adress'    => ['default' => '?'],
    ]);

    if ($validator->errors()) {
        Flash::set('errors', implode('<br>', $validator->errors()), 'danger');

        return Redirect::back();
    }

    if (isset($_POST['name']) && isset($_POST['firstName'])) {
        if ($id !== false) {
            $valide = Personne::update($id, $_POST) !== false;
        } else {
            $valide = Personne::create($_POST) !== false;
        }
    }

    if ($valide) {
        Flash::set('modify', "L'opération s'est déroulée avec succès", 'success');
    } else {
        Flash::set('modify', "Une erreur est survenue", 'danger');
    }

    return Redirect::make('personne/');
});
