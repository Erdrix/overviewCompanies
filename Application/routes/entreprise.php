<?php

/**
 * Configuration du router
 * @template  : Template à utiliser lors des rendus
 * @variables : Variable pré-définis pour les vues
 */
Router::config([
    'variables'  => ['navigation' => 'entreprise'],
    'template'   => 'global',
    'middleware' => ['authorization'],
]);

/**
 * Interface de listing des entreprises
 * @route : get:entreprise/
 */
Router::respond('GET', '/', function (Auth $auth) {
    return View::make('entreprise.index', ['title' => 'Recherche d\'entreprise', 'admin' => $auth->logged()]);
});

/**
 * Interface d'information sur une entreprise
 * @route : get:entreprise/view
 */
Router::respond('GET', '/view/{id}', function ($id) {
    // Récupération des informations
    $entreprise = Entreprise::find($id);
    $stages     = Stage::byEntreprise($id);
    $filieres   = Filiere::identifiants(['name', 'color']);
    $taxes      = Taxe::byEntreprise($id);

    // Protection des variables
    $entreprise = array_escape($entreprise);

    // Création des variables de stockage
    $data   = [];
    $date   = [];
    $annees = ['All'];

    // Traitement des informations
    $temp = null;
    foreach ($stages as $stage) {
        $temp     = substr($stage->startAt, 0, 4);
        $date[]   = $temp;
        $data[]   = [$filieres[$stage->idField]->name, $temp, $stage->study];
        $annees[] = $stage->study;
    }

    // Récupération des taxes
    $list_taxe = [];
    foreach ($taxes as $taxe) {
        $list_taxe[$taxe->year] = $taxe->amount;
    }
    ksort($list_taxe);

    // Gestion des couleurs
    $colors = [];
    foreach ($filieres as $filiere) {
        $colors[$filiere->name] = '#' . $filiere->color;
    }

    // Gestion du nom des filieres
    $fili = [];
    foreach ($data as $value) {
        $fili[] = $value[0];
    }

    // Nombre de stage moyen
    $fili    = array_unique($fili);
    $average = count($stages);
    $average = round($average / count(array_unique($date)), 1);

    $params = [
        'title'      => 'Entreprise ' . $entreprise->name,
        'entreprise' => $entreprise,
        'filieres'   => $filieres,
        'fili'       => $fili,
        'colors'     => $colors,
        'annees'     => array_keys(array_count_values($annees)),
        'date'       => array_keys(array_count_values($date)),
        'average'    => $average,
        'data'       => $data,
        'taxes'      => $list_taxe,
    ];

    return View::make('entreprise.view', $params);
});


/**
 * Interface d'information sur une entreprise
 * @route : get:entreprise/view
 */
Router::respond('GET', '/overview', function () {
    // Récupération des informations
    $entreprise    = Entreprise::all();
    $secteur       = Sector::all();
    $entreprise    = array_escape($entreprise);
    $dataCompanies = [];
    $date_temp     = [];
    $list_stage    = [];
    $list_taxe     = [];
    $temp          = null;
    $temp2         = null;
    $nbstages1     = [];
    $nbstages2     = [];
    $entreprise1   = [];
    $entreprise2   = [];
    $entreprise3   = [];
    $montant       = [];
    $top           = [];
    $cpt           = 0;

    foreach ($entreprise as $key1 => $value1) {
        $cpt++;

        /*Liste des stages et taxes de l'entreprise*/
        $stages = Stage::byEntreprise($value1->id);
        $taxes  = Taxe::byEntreprise($value1->id);

        /*Raz du tableau de dates*/
        unset($list_stage);
        unset($list_taxe);
        unset($date_temp);

        /*Traitement du nombre de stage par année*/
        foreach ($stages as $stage) {
            $temp = substr($stage->startAt, 0, 4);

            /*Si c'est la premère fois que l'on recontre une année*/
            if (!array_key_exists($temp, $list_stage)) {
                $list_stage[$temp] = 1;
            } else {
                $list_stage[$temp] += 1;
            } // Sinon on ajoute un stage.
        }
        ksort($list_stage);

        /*Traitement des taxes versée par l'entreprise*/
        foreach ($taxes as $taxe) {
            $list_taxe[$taxe->year] = $taxe->amount;
        }
        ksort($list_taxe);

        /*On mémorise toutes les dates*/
        foreach ($list_taxe as $key => $value) {
            $date_temp[] = $key;
        }

        foreach ($list_stage as $key => $value) {
            if (!in_array($key, $date_temp)) {
                $date_temp[] = $key;
            }
        }
        ksort($date_temp);

        /*Création de dataCompanies*/
        foreach ($date_temp as $key2) {
            $temp                   = (array_key_exists($key2, $list_stage)) ? $list_stage[$key2] : 0;
            $temp2                  = (array_key_exists($key2, $list_taxe)) ? $list_taxe[$key2] : 0;
            $dataCompanies[$key2][] =
                [
                    'idCompanies' => $value1->id,
                    'name'        => $value1->name,
                    'nbstages'    => $temp,
                    'amount'      => $temp2,
                    'sector'      => $value1->sectors_name,
                ];
        }
    }

    ksort($dataCompanies);

    /*Initialisation du Top 5*/
    foreach ($dataCompanies as $key => $value) {
        /*Initialisation du TOP*/
        for ($i = 0; $i < 5; $i++) {
            $top[$key][$i] = [
                'name'   => '?',
                'amount' => 0
            ];
        }

        foreach ($value as $key2 => $value2) {
            /*Calcul des sommes total versées chaque année*/
            if (!array_key_exists($key, $montant)) {
                $montant[$key]['All']             = $value2['amount'];
                $montant[$key][$value2['sector']] = $value2['amount'];
            } elseif (!array_key_exists($value2['sector'], $montant[$key])) {
                $montant[$key][$value2['sector']] = $value2['amount'];
                $montant[$key]['All'] += $value2['amount'];
            } else {
                $montant[$key][$value2['sector']] += $value2['amount'];
                $montant[$key]['All'] += $value2['amount'];
            }

            /*Création du Top 5*/
            $i = 0;
            while ($i < 5) {
                if ($value2['amount'] > $top[$key][$i]['amount']) {
                    $switch_value = [
                        'name'   => $top[$key][$i]['name'],
                        'amount' => $top[$key][$i]['amount']
                    ];

                    $top[$key][$i] = [
                        'name'   => $value2['name'],
                        'amount' => $value2['amount'],
                    ];
                    for ($j = $i + 1; $j < 5; $j++) {
                        $switch_temp   = [
                            'name'   => $top[$key][$j]['name'],
                            'amount' => $top[$key][$j]['amount']
                        ];
                        $top[$key][$j] = [
                            'name'   => $switch_value['name'],
                            'amount' => $switch_value['amount'],
                        ];
                        $switch_value  = $switch_temp;
                    }
                    $i = 5;
                }
                $i++;
            }

            /*Calcul du nombre de stage total et d'entreprise par année trié selon 3 catégories*/
            if (($value2['amount'] == 0) && ($value2['nbstages'] != 0)) // Entreprise avec stage sans Taxe.
            {
                $entreprise1[$key]['All']             = (array_key_exists($key, $entreprise1)) ?
                    $entreprise1[$key]['All'] + 1 : 1;
                $nbstages1[$key]['All']               = (array_key_exists($key, $nbstages1)) ?
                    $nbstages1[$key]['All'] + $value2['nbstages'] : $value2['nbstages'];
                $entreprise1[$key][$value2['sector']] = (array_key_exists($value2['sector'], $entreprise1[$key])) ?
                    $entreprise1[$key][$value2['sector']] + 1 : 1;
                $nbstages1[$key][$value2['sector']]   = (array_key_exists($value2['sector'], $nbstages1[$key])) ?
                    $nbstages1[$key][$value2['sector']] + $value2['nbstages'] : $value2['nbstages'];
            } elseif (($value2['amount'] != 0) && ($value2['nbstages'] == 0)) // Entreprise avec Taxe sans stage.
            {
                $entreprise2[$key]['All']             = (array_key_exists($key, $entreprise2)) ?
                    $entreprise2[$key]['All'] + 1 : 1;
                $entreprise2[$key][$value2['sector']] = (array_key_exists($value2['sector'], $entreprise2[$key])) ?
                    $entreprise2[$key][$value2['sector']] + 1 : 1;
            } elseif (($value2['amount'] != 0) && ($value2['nbstages'] != 0)) // Entreprise avec Taxe et avec stage.
            {
                $entreprise3[$key]['All']             = (array_key_exists($key, $entreprise3)) ?
                    $entreprise3[$key]['All'] + 1 : 1;
                $nbstages2[$key]['All']               = (array_key_exists($key, $nbstages2)) ?
                    $nbstages2[$key]['All'] + $value2['nbstages'] : $value2['nbstages'];
                $entreprise3[$key][$value2['sector']] = (array_key_exists($value2['sector'], $entreprise3[$key])) ?
                    $entreprise3[$key][$value2['sector']] + 1 : 1;
                $nbstages2[$key][$value2['sector']]   = (array_key_exists($value2['sector'], $nbstages2[$key])) ?
                    $nbstages2[$key][$value2['sector']] + $value2['nbstages'] : $value2['nbstages'];
            }
        }
    }
    //dd($top);
    $params = [
        'title'       => 'OverView',
        'versement'   => $montant,
        'top'         => $top,
        'type_ent1'   => $entreprise1,  // avec stage et sans Taxe.
        'type_stage1' => $nbstages1,
        'type_ent2'   => $entreprise2,  // avec taxe et sans stage.
        'type_ent3'   => $entreprise3,  // avec stage et taxe.
        'type_stage3' => $nbstages2,
        'secteur'     => $secteur,
        'navigation'  => 'overview',
    ];

    return View::make('entreprise.overview', $params);
});


/**
 * Interface de création d'entreprise
 * @route : get:entreprise/add
 */
Router::respond('GET', '/add', function () {
    $sector = Sector::identifiants(['name']);

    return View::make('entreprise.update', [
        'title'  => 'Ajout d\'une entreprise',
        'sector' => $sector,
        'form'   => new FormBuilder()
    ]);
});

/**
 * Interface de mise à jour d'une entreprise
 * @route : get:entreprise/modify
 */
Router::respond('GET', '/modify/{id}', function ($id) {
    $entreprise = Entreprise::find($id);
    $sector     = Sector::identifiants(['name']);

    $params = [
        'title'  => 'Modification de l\'entreprise ' . $entreprise->name,
        'sector' => $sector,
        'form'   => new FormBuilder($entreprise),
    ];

    return View::make('entreprise.update', $params);
});

/**
 * Interface de suppression d'une entreprise
 * @root : get:entreprise/askdelete
 */
Router::respond('GET', '/askdelete/{id}', function ($id) {
    $entreprise = Entreprise::find($id);
    $params     = [
        'title'      => 'Suppression de l\'entreprise ' . $entreprise->name,
        'entreprise' => $entreprise,
    ];

    return View::make('entreprise.askdelete', $params);
});

/**
 * Suppression d'une entreprise
 * @root : post:entreprise/delete
 */
Router::respond('GET', '/delete/{id}', function ($id) {
    $valide = Entreprise::delete($id) !== false;

    if (!$valide) {
        Flash::set('modify', 'Impossible de supprimer l\'entreprise numéro ' . $id, 'danger');
    } else {
        Flash::set('modify', 'L\'entreprise numéro ' . $id . ' vient d\'être supprimée', 'success');
    }

    return Redirect::make('entreprise');
});

/**
 * Récupération de la liste des entreprises
 * @route : post:entreprise/request
 */
Router::respond('POST', '/request', function (Auth $auth) {
    $data = false;
    if (isset($_POST['data']) && '*' != $_POST['data']) {
        $link = [
            't' => 'phone',
            'p' => 'country',
            'a' => 'adress',
            's' => 'ovc_sectors.name',
        ];

        $temp = explode('@', $_POST['data']);
        $data = [];
        foreach ($temp as $part) {
            $part = trim($part);
            $temp = explode(':', $part);
            if (isset($temp[1]) && isset($link[$temp[0]])) {
                $data[$link[$temp[0]]] = '%' . $temp[1] . '%';
            } else {
                $data['name'] = '%' . $part . '%';
            }

        }
    }

    $data   = $data ? Entreprise::finds($data) : Entreprise::all();
    $data   = array_escape($data);
    $result = '';
    $admin  = $auth->logged();
    foreach ($data as $entreprise) {
        $result .= '<tr class = "tbl-item">';
        $result .= '	<th scope="row" class = "nameEnt"><a href="?url=entreprise/view/' . $entreprise->id . '/">' . $entreprise->name .
                   '</a></th>';
        $result .= '<td class = "sectorEnt">' . $entreprise->sectors_name . '</td>';
        $result .= '<td class = "phoneEnt">' . $entreprise->phone . '</td>';
        $result .= '<td class = "citiesEnt">' . $entreprise->adress . '<br>' . $entreprise->cities_name . '<br>' . $entreprise->cities_zip .
                   '</td>';
        $result .= '<td class = "countryEnt">' . $entreprise->country . '</td>';
        if ($admin) {
            $result .= '<td>';
            $result .= ' <a href="?url=entreprise/modify/' . $entreprise->id .
                       '/" class="btn btn-primary">Modifier</a>';
            $result .= ' <a href="?url=entreprise/askdelete/' . $entreprise->id .
                       '/" class="btn btn-danger">Supprimer</a>';
            $result .= '</td>';
        }
        $result .= '</tr>';
    }

    return $result;
});

/**
 * Mise à jour d'une entreprise
 * @route : post:entreprise/update
 */
Router::respond('POST', '/modify', function (Validator $validator) {
    $valide = false;
    $id     = isset($_POST['id']) ? $_POST['id'] : false;
    unset($_POST['id']);

    $validator->check($_POST, [
        'name'     => ['required' => true],
        'idSector' => ['required' => true],
        'idCity'   => ['default' => 1],
        'phone'    => ['default' => '0000000000'],
        'mail'     => ['default' => '?'],
        'adress'  => ['default' => '?'],
        'country'  => ['default' => '?'],
    ]);

    if ($validator->errors()) {
        Flash::set('errors', implode('<br>', $validator->errors()), 'danger');

        return Redirect::back();
    }

    if (isset($_POST['name'])) {
        if ($id !== false) {
            $valide = Entreprise::update($id, $_POST) !== false;
        } else {
            $valide = Entreprise::create($_POST) !== false;
        }
    }

    if ($valide) {
        Flash::set('modify', "L'opération s'est déroulée avec succès", 'success');
    } else {
        Flash::set('modify', "Une erreur est survenue", 'danger');
    }

    return Redirect::make('entreprise/');
});