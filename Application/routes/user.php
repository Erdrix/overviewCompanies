<?php

/**
 * Configuration du router
 * @template  : Template à utiliser lors des rendus
 * @variables : Variable pré-définis pour les vues
 */
Router::config([
    'variables' => ['title' => 'Utilisateur'],
    'template'  => 'global',
]);

/**
 * A définir
 * @route : get:user/
 */
Router::respond('GET', '/', function (Auth $auth) {
    if (!$auth->logged()) {
        return Redirect::make('user/login');
    }

    return View::make('user.index', ['user' => $auth->user()]);
});

/**
 * Interface de connexion
 * @route : get:user/login
 */
Router::respond('GET', '/login', function (Auth $auth) {
    if ($auth->logged()) {
        return Redirect::make('user/');
    }

    return View::make('user.login', ['form' => new FormBuilder()]);
});

/**
 * Déconnexion utilisateur
 * @route : get:user/disconnect
 */
Router::respond('GET', '/disconnect', function (Auth $auth) {
    $auth->logout();

    return Redirect::make('user/login');
});

/**
 * Connexion utilisateur
 * @route : post:user/login
 */
Router::respond('POST', '/login', function (Auth $auth) {
    if (empty($_POST) || !$auth->login($_POST['username'], $_POST['password'])) {
        Flash::set('login', 'Nom d\'utilisateur ou mot de passe invalide !', 'danger');

        return Redirect::make('user/login');
    }

    Flash::set('modify', 'Vous êtes maintenant connecté !', 'success');
    CSRFToken::generate();

    return Redirect::make('');
});