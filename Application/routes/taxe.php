<?php

/**
 * Configuration du router
 * @template  : Template à utiliser lors des rendus
 * @variables : Variable pré-définis pour les vues
 */
Router::config([
    'variables'  => ['navigation' => 'taxe'],
    'template'   => 'global',
    'middleware' => ['authorization'],
]);


/**
 * Interface de listing des taxes
 * @route : get:taxe/
 */
Router::respond('GET', '/', function (Auth $auth) {
    return View::make('taxe.index', ['title' => 'Liste des taxes', 'auth' => $auth]);
});

/**
 * Interface de listing des taxes
 * @route : get:taxe/
 */
Router::respond('GET', '/entreprise/{ent}', function (Auth $auth, $ent) {
    return View::make('taxe.index', ['title' => 'Liste des taxes', 'auth' => $auth, "ent" => $ent]);
});

/**
 * Interface de création d'une taxe
 * @route : get:taxe/add
 */
Router::respond('GET', '/add', function () {
    $entreprises = Entreprise::identifiants(['name']);
    $sector        = Sector::identifiants(['name']);

    $params = [
        'title'       => 'Création de taxe',
        'sector'      => $sector,
        'entreprises' => $entreprises,
        'form'        => new FormBuilder(),
    ];

    return View::make('taxe.update', $params);
});

/**
 * Interface de mise à jour d'une taxe
 * @route : get:taxe/modify
 */
Router::respond('GET', '/modify/{id}', function ($id) {
    $data['id']    = $id;
    $data['taxe']  = Taxe::find($id);
    $sector        = Sector::identifiants(['name']);
    $entreprises   = Entreprise::identifiants(['name']);

    $params = [
        'title'       => 'Modification de la taxe ' . $data['taxe']->id,
        'entreprises' => $entreprises,
        'sector'      => $sector,
        'form'        => new FormBuilder($data),
    ];

    return View::make('taxe.update', $params);
});
/**
 * Interface de suppression d'une taxe
 * @root : get:taxe/askdelete
 */
Router::respond('GET', '/askdelete/{id}', function ($id) {
    $taxe  = Taxe::find($id);
    $params = [
        'title' => 'Suppression d\'une taxe ' . $taxe->id,
        'taxe' => $taxe,
    ];

    return View::make('taxe.askdelete', $params);
});

/**
 * Suppression d'une taxe
 * @root : post:taxe/delete
 */
Router::respond('GET', '/delete/{id}', function ($id) {
    $taxe  = Taxe::find($id);
    $valide = Taxe::delete($id) !== false;

    $params = [
        'title'   => 'Suppression d\'une taxe',
        'id'      => $id,
        'message' => $valide ? ('Le taxe n°' . $taxe->id .
                                ' vient d\'être supprimée') : 'La taxe ne peut être supprimée',
        'fail'    => !$valide,

    ];

    return View::make('taxe.confirm', $params);
});

/**
 * Récupération de la liste des taxes
 * @route : post:taxe/request
 */
Router::respond('POST', '/request', function (Auth $auth) {
    $data = false;
    if (isset($_POST['data']) && '*' != $_POST['data']) {
        $link = [
            'e' => 'ovc_companies.name',
            'm' => 'amount',
            'a' => 'year',
        ];

        $temp = explode('@', $_POST['data']);
        $data = [];
        foreach ($temp as $part) {
            $part = trim($part);
            $temp = explode(':', $part);
            if (isset($temp[1]) && isset($link[$temp[0]])) {
                $data[$link[$temp[0]]] = '%' . $temp[1] . '%';
            } else {
                $data['id'] = '%' . $part . '%';
            }
        }
    }

    $taxes = $data ? Taxe::finds($data) : Taxe::all(50);
    $taxes = array_escape($taxes);
    $result = '';
    $admin  = $auth->logged();

    foreach ($taxes as $key => $value) {
        $result .= '<tr class = "tbl-item">';
        $result .= '<th class = "id" scope="row">' . $value->id . '</th>';

        $result .= '<td class = "companies">' . (($value->idCompany !== null) ? $value->companies_name : '?') . '</td>';
        $result .= '<td class = "amount">'. $value->amount .'</td>';
        $result .= '<td class = "year">'. $value->year .'</td>';
        if ($admin) {
            $result .= '<td>';
            $result .= ' <a href="?url=taxe/modify/' . $value->id . '/" class="btn btn-primary">Modifier</a>';
            $result .= ' <a href="?url=taxe/askdelete/' . $value->id . '/" class="btn btn-danger">Supprimer</a>';
            $result .= '</td>';
        }
        $result .= '</tr>';
    }

    return $result;
});

/**
 * Mise à jour d'une entreprise
 * @route : post:entreprise/update
 */
Router::respond('POST', '/modify', function (Validator $validator) {
    $id     = isset($_POST['id']) ? $_POST['id'] : false;
    $taxe  = $_POST['taxe'];
    $valide = false;
    unset($_POST['id']);

    /*$validator->check($_POST, [
        'taxe' => ['required' => true],
    ]);*/

    if ($validator->errors()) {
        Flash::set('errors', implode('<br>', $validator->errors()), 'danger');

        return Redirect::back();
    }

     // Création d'une entreprise
    if ($taxe['idCompany'] === "-1" && isset($_POST['entreprise'])) {
        $taxe['idCompany'] = Entreprise::create($_POST['entreprise']);
    }

    // Création ou modification de l'entreprise
    if ($taxe['idCompany'] !== false) {
        if ($id !== false) {
            $valide = Taxe::update($id, $taxe) !== false;
        } else {
            $valide = Taxe::create($taxe) !== false;
        }
    }

    if ($valide) {
        Flash::set('modify', "L'opération s'est déroulée avec succès", 'success');
    } else {
        Flash::set('modify', "Une erreur est survenue", 'danger');
    }

    return Redirect::make('taxe/');
});
