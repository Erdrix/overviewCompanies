<?php

return [

    ###############################
    # Fichier de configuration des session
    ################
    'session' => [

        ###############
        # name : Nom du cookie de sauvegarde des sessions
        #########
        'name'     => 'fram_sid',

        ###############
        # duration : Durée de vie des session en seconds
        #########
        'duration' => 3600,

        ###############
        # cookie : Durée du cookie en second (0 pour la durée de la session )
        #########
        'cookie'   => 0,

        ###############
        # path : Nom du dossier de sauvegarde des sessions
        #         Désactivé si false
        #########
        'path'     => false,
    ],
];