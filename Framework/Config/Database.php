<?php

return [

    ###############################
    # Fichier de configuration de la base de donnée
    ################
    'database' => [

        ###############
        # db_type : Type de base de donnée
        # db_name : Nom de la base de donnée
        # host    : Adresse de l'hote
        # user    : Utilisateur
        # pass    : Mot de passe
        # prefix  : Prefix des tables
        # charset : Jeu de caractères
        #########
        'db_type'         => 'mysql',
        'db_name'         => 'polypro',
        'host'            => 'localhost',
        'user'            => 'root',
        'password'        => '',
        'prefix'          => 'ovc_',
        'charset'         => 'utf8',

        ###############
        # debug_level: Erreur SQL (0 => Silent, 1 => Warning, 2 => Exception]
        #########
        'debug_level'     => 1,

        ###############
        # cache_directory : Dossier de mise en cache (tmp/cache/{cache_directory})
        #########
        'cache_directory' => 'sql',

        ###############
        # cache_duration : Durée du cache en minutes (0 pour désactiver, 1440 pour une journée)
        #########
        'cache_duration'  => 0,

    ],
];
