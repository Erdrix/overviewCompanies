<?php

return [

    ###############################
    # Fichier de configuration du parser
    ################
    'parser' => [

        ###############
        # cache_directory : Dossier de mise en cache (tmp/cache/{cache_directory})
        #########
        'cache_directory' => 'pages',

        ###############
        # cache_duration : Durée du cache en minutes (0 pour désactiver, 1440 pour une journée)
        #########
        'cache_duration'  => 0,

        ###############
        # rules : Rêgles de parsing du texte
        #   'regex' => 'php'
        #########
        'rules'           => [
            '\{\{(.+?)\}\}'                                  => '<?= ${1}; ?>',
            '(\s*)@(else)(\s*)'                              => '$1<?php $2: ?>$3',
            '(\s*)@(require)(\s*\(.*\))'                     => '$1<?php Application::request$3; ?>',
            '(\s*)@(endif|endforeach|endfor|endwhile)(\s*)'  => '$1<?php $2; ?>$3',
            '(\s*)@(if|elseif|foreach|for|while)(\s*\(.*\))' => '$1<?php $2$3: ?>',
        ],

    ],

];
