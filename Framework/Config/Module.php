<?php

return [

    ###############################
    # Liste des modules à charger
    ################
    'modules'     => [
        'LoggerProvider',
        'SessionProvider',
        'DatabaseProvider',
        'BenchmarkProvider',
        'ParserProvider',
    ],

    ###############################
    # Liste des middleware à charger
    ################
    'middlewares' => [
        'Csrf',
    ],

];
