<?php

return [

    ###############################
    # Fichier des configurations global
    ################
    'global' => [

        ###############
        # Default:
        #    Route : Route par défaut
        #########
        'default'     => [
            'route' => 'entreprise/overview',
        ],

        ###############
        # Url_rewrite : Activation de l'url rewriting
        #########
        'url_rewrite' => true,

        ###############
        # Permission:
        #     Folders : Permission lors de la création de dossiers
        #     Files   : Permission lors de la création de fichiers
        #########
        'permission'  => [
            'folders' => 777,
            'files'   => 777,
        ],

        ###############
        # Debug_level : Erreur Php ( http://php.net/manual/fr/errorfunc.constants.php )
        #########
        'debug_level' => E_ALL | E_PARSE, //E_ERROR | E_WARNING | E_PARSE | E_NOTICE,

        ###############
        # Logs : Activer ou désactiver la sauvegarde des logs
        #########
        'logs'        => false,

        ###############
        # Charset : Charset utilisé lors de l'envois des en-tête html
        #########
        'charset'     => 'utf-8',

        ###############
        # Benchmark : Active ou désactive le benchmarking des pages visitées
        #########
        'benchmark'   => true,

        ###############
        # Table_user : Nom de la table contenant les utilisateurs (false pour désactiver)
        #########
        'table_user' => 'users',
    ],
];
