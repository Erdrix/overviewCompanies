<?php

class DatabaseProvider implements Provider
{

    /**
     * Enregistrement de la base de donnée
     */
    public static function register()
    {
        Container::singleton('Database', function () {
            return new MysqlDatabase(Conf::get('database'));
        });
    }
}