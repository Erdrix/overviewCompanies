<?php

class Parser
{

    //////////////////////////
    // Attributs
    /////////

    /**
     * Rêgle de parsing
     * @var array
     */
    private static $parsing = [];

    /**
     * Mise en cache des pages
     * @var Cache
     */
    private static $cache;

    //////////////////////////
    // Static Methods
    /////////

    /**
     * Parse une page suivant les informations pré-enregistré
     *
     * @param        $path     : Chemin de la page
     * @param array  $params   : Parametres de la page
     * @param string $template : Template de la page
     *
     * @return string
     */
    public static function parsePage($path, $params = [], $template = '')
    {
        $hash = hash('md2', $path);
        if (!isset(self::$cache) || !self::$cache->inCache($hash)) {
            if (!is_file($path)) {
                trigger_error('Impossible de trouver la vue ' . $path);
            }

            $template_content = file_get_contents($path);
            if ($template) {
                $template_layout  = file_get_contents(APP . 'views/__template/' . $template . '.php');
                $template_content = str_replace('@template@', $template_content, $template_layout);
                unset($template_layout);
            }

            if (isset(self::$cache)) {
                self::$cache->write($hash, self::parse($template_content), false);
            }
        }

        ob_start();
        extract($params);
        require(getTemporary('cache/pages') . $hash);

        return ob_get_clean();
    }

    /**
     * Définis les rêgles de mise en cache
     *
     * @param string  $directory : Repertoire de sauvegarde du cache
     * @param integer $duration  : Durée de mise en cache
     */
    public static function setCache($directory, $duration)
    {
        self::$cache = new Cache($directory, $duration);
    }

    /**
     * Initialise une nouvelle rêgle
     *
     * @param string $search  : Regex de remplacement
     * @param string $replace : Texte à remplacer par...
     */
    public static function replace($search, $replace)
    {
        self::$parsing[$search] = $replace;
    }

    /**
     * Retourne le texte parsé suivant les rêgles initialisées
     *
     * @param  string $content : Texte à parser
     *
     * @return string : Texte parsé
     */
    private static function parse($content)
    {
        foreach (self::$parsing as $search => $replace) {
            $content = preg_replace('/' . $search . '/', $replace, $content);
        }

        return $content;
    }

}
