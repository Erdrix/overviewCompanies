<?php

class ParserProvider implements Provider
{

    public static function register()
    {
        $conf = Conf::get('parser');
        Parser::setCache($conf['cache_directory'], $conf['cache_duration']);
        foreach ($conf['rules'] as $rule => $value) {
            Parser::replace($rule, $value);
        }

        // Lors du rendu d'une vue
        Dispatcher::listen('view.render', function ($event) {
            $event->content   = Parser::parsePage($event->path, $event->params, $event->template);
            $event->cancelled = true;

            return $event;
        }, 1);
    }
}