<!DOCTYPE html>
<html lang="fr">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>Page d'erreur</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/error.css" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Scripts -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
    <script src="js/error.js"></script>
    <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?callback=target"></script>
</head>
<body>

<div class='menu'>
    <?php
    $id = 0;
    foreach ($traces as $trace) {
        if ($trace['file'] !== false) {
            echo '<div class="menu_box ' . ($id === 0 ? 'active' : '') . '" id=' . $id++ . '>';
            echo '<strong>' . $trace['function'] . '</strong><br>';
            echo str_replace(ROOT, '...', $trace['file']) . ':' . $trace['line'];
            echo '</div>';
        }
    }
    ?>
</div>

<div class='content'>
    <div class='header'>
        <span class='title'>Exception</span>
        <span class='joke'>Oups, vous semblez avoir cassé quelque chose !</span>
    </div>

    <?php
    $id = 0;
    foreach ($traces as $trace) {
        if ($trace['file'] !== false) {
            // Récupération du contenu du fichier
            $data = file_get_contents($trace['file']);
            $data = explode("\n", $data);

            // Configuration de la longueur de l'output
            $length = 8;

            // Récupération du numéro des lignes
            $total = count($data);
            $min   = ($trace['line'] - $length) < 0 ? 0 : ($trace['line'] - $length);
            $max   = ($trace['line'] + $length) > $total ? $total : ($trace['line'] + $length);

            // Récupération des lignes à afficher
            $data = array_slice($data, $min, $max, true);
            ?>

            <div class="data <?= $id == 0 ? 'current' : ''; ?> box-<?= $id++; ?>">
                <div class='file'>
                    <div class='code'>
                        <div class='title'>#<?= $trace['line'] ?> <?= $trace['file'] ?></div>
                        <pre class='prettyprint theme-tonic linenums:<?= $min + 1; ?>' id='<?= $trace['line'] ?>'
                             style='word-break: break-word;'><?php
                            for ($i = $min; $i < $max; $i++) {
                                echo htmlspecialchars($data[$i]);
                            }
                            ?></pre>
                    </div>
                </div>

                <div class="debug">
                    <?php if (!empty($trace['args'])) { ?>
                        <span class='type'>Arguments</span>
                        <div class='props'>
                            <?php
                            $nb = 1;
                            foreach ($trace['args'] as $value) {
                                echo '<span>Argument n°' . $nb++ . '</span> ' . $value . '<br>';
                            }
                            ?>
                        </div>
                    <?php } ?>
                    <span class='type'>Informations</span>

                    <div class='props'>
                        <span>Méthode</span> <?= $page['method'] ?><br>
                        <span>Adresse</span> <?= $page['uri'] ?><br>
                        <span>Requête</span> <?= $page['query'] ?><br>
                        <span>Serveur</span> <?= $page['software'] ?><br>
                        <span>Ip</span> <?= $page['adresse'] ?><br>
                    </div>
                </div>

            </div>

        <?php
        }
    }
    ?>
</div>

</body>
</html>