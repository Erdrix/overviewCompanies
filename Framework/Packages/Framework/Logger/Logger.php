<?php

class Logger
{

    //////////////////////////
    // Attributs
    /////////

    /**
     * Droit sur la création des fichiers
     * @var integer
     */
    private $permission;

    /**
     * Chemin vers le dossier de log
     * @var mixed
     */
    private $path;

    //////////////////////////
    // Constructeur
    /////////

    public function __construct()
    {
        $conf             = Conf::get('global');
        $this->path       = $conf['logs'] ? getTemporary('logs') : false;
        $this->permission = $conf['permission']['files'];

        register_shutdown_function([$this, '__shutdown']);
        set_exception_handler([$this, '__dispatcher']);
        set_error_handler([$this, '__error']);
        ini_set('display_errors', 'off');
    }

    //////////////////////////
    // Public methods
    /////////

    /**
     * Intercepte les erreurs critiques
     */
    public function __shutdown()
    {
        $last_error = error_get_last();
        if (E_ERROR === $last_error['type'] || 4 === $last_error['type']) {
            $this->__error($last_error['type'], $last_error['message'], $last_error['file'], $last_error['line']);
        }
    }

    /**
     * Intercepte les erreurs non critiques
     *
     * @param integer $code    : Code de l'erreur
     * @param string  $message : Message de l'erreur
     * @param string  $file    : Chemin vers le fichier provoquant l'erreur
     * @param integer $line    : Numéro de la ligne provoquant l'erreur
     */
    public function __error($code, $message, $file, $line)
    {
        if (E_ERROR === $code || 4 === $code) {
            $e = new ErrorException($message, $code, $code, $file, $line);
            $this->__dispatcher($e);
        }
    }

    /**
     * Intercepte les exception et les sauvegarde
     *
     * @param Exception $e
     */
    public function __dispatcher(Exception $e)
    {
        // Log error here
        if ($this->path) {
            $path = $this->path . DS . date('m-d-Y') . '.log';
            file_put_contents($path,
                '[' . date('H:i:s') . '] Erreur dans le fichier ' . $e->getFile() . ' a la ligne ' . $e->getLine() . "\n           " . $e->getMessage() . "\n",
                FILE_APPEND);
            chmod($path, octdec('0' . $this->permission));
        }

        // Save data
        $traces[] = $this->generateTrace($e);
        foreach ($e->getTrace() as $trace) {
            $traces[] = $this->generateTrace($trace);
        }

        // Suppresion du code
        while (ob_get_level() > 0) {
            ob_end_clean();
        }

        // Affichage des erreurs
        Application::render('@framework.logger/error', ['traces' => $traces, 'page' => $this->getPageStats()]);

        // Fin du script
        die();
    }

    //////////////////////////
    // Private
    /////////

    /**
     * Génere une trace valide de l'erreur
     *
     * @param $trace : Trace à valider
     *
     * @return array
     */
    private function generateTrace($trace)
    {
        if ($trace instanceof Exception) {
            return [
                'file'     => $trace->getFile(),
                'line'     => $trace->getLine(),
                'class'    => get_class($trace),
                'args'     => [$trace->getMessage()],
                'function' => get_class($trace),
            ];
        }

        return [
            'file'     => isset($trace['file']) ? $trace['file'] : false,
            'line'     => $trace['line'],
            'class'    => isset($trace['class']) ? $trace['class'] : false,
            'args'     => $this->generateSerializable($trace['args']),
            'function' => (isset($trace['class']) && strpos($trace['function'],
                    '{') === false) ? ($trace['class'] . $trace['type'] . $trace['function']) : ucfirst($trace['function']),
        ];
    }

    /**
     * Prépare un tableau pour une sérialisation
     *
     * @param array $array : Tableau a préparer pour une sérialisation
     *
     * @return array
     */
    private function generateSerializable($array)
    {
        $result = [];

        foreach ($array as $value) {
            $type = gettype($value);
            if ($value instanceof Closure) {
                $reflection = new ReflectionFunction($value);
                $arguments  = $reflection->getParameters();
                $list       = [];
                foreach ($arguments as $param) {
                    $list[] = $param->getName();
                }

                if (!empty($list)) {
                    $value .= 'Closure (<span style="color:orange">$' . implode(', $', $list) . '</span>)';
                }
            } elseif ('object' === $type) {
                $value = 'Class (<span style="color:orange">' . get_class($value) . '</span>)';
            } elseif (is_array($value)) {
                $value = $this->generateSerializable($value);
            }

            $result[] = $value;
        }

        return $result;
    }

    /**
     * Retoure les informations sur la page
     * @return array
     */
    private function getPageStats()
    {
        return [
            'method'   => $_SERVER['REQUEST_METHOD'],
            'software' => $_SERVER['SERVER_SOFTWARE'],
            'adresse'  => $_SERVER['REMOTE_ADDR'],
            'uri'      => $_SERVER['REQUEST_URI'],
            'query'    => $_SERVER['QUERY_STRING'],
        ];
    }
}
