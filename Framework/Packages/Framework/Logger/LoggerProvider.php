<?php

class LoggerProvider implements Provider
{

    public static function register()
    {
        Container::instance(new Logger());

        // Redirection des erreurs 404 provoqué par le router
        Dispatcher::listen('router.error', function () {
            Application::render('@framework.logger/404');
            die();
        });
    }
}