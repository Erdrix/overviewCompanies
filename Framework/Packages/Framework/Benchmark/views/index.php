<style>
	*,
	*:before,
	*:after {
	  -moz-box-sizing: border-box;
	  -webkit-box-sizing: border-box;
	  box-sizing: border-box;
	}
	.skillbar
	{
		position:relative;
		display:block;
		margin-bottom:15px;
		width:100%;
		background:#eee;
		height:35px;
		border-radius:3px;
		-moz-border-radius:3px;
		-webkit-border-radius:3px;
		-webkit-transition:0.4s linear;
		-moz-transition:0.4s linear;
		-ms-transition:0.4s linear;
		-o-transition:0.4s linear;
		transition:0.4s linear;
		-webkit-transition-property:width, background-color;
		-moz-transition-property:width, background-color;
		-ms-transition-property:width, background-color;
		-o-transition-property:width, background-color;
		transition-property:width, background-color;
	}
	.skillbar-title
	{
		position:absolute;
		top:0;
		left:0;
		font-weight:bold;
		font-size:13px;
		color:#fff;
		background:#1C70D0;
		-webkit-border-top-left-radius:3px;
		-webkit-border-bottom-left-radius:4px;
		-moz-border-radius-topleft:3px;
		-moz-border-radius-bottomleft:3px;
		border-top-left-radius:3px;
		border-bottom-left-radius:3px;
	}
	.skillbar-title span
	{
		display:block;
		background:rgba(0, 0, 0, 0.1);
		padding:0 20px;
		height:35px;
		line-height:35px;
		-webkit-border-top-left-radius:3px;
		-webkit-border-bottom-left-radius:3px;
		-moz-border-radius-topleft:3px;
		-moz-border-radius-bottomleft:3px;
		border-top-left-radius:3px;
		border-bottom-left-radius:3px;
	}
	.skillbar-bar
	{
		height:35px;
		width:0px;
		background:#2A8FCD;
		border-radius:3px;
		-moz-border-radius:3px;
		-webkit-border-radius:3px;
	}
	.skill-bar-percent
	{
		position:absolute;
		right:10px;
		top:0;
		font-size:11px;
		height:35px;
		line-height:35px;
		color:#444;
		color:rgba(0, 0, 0, 0.4);
	}
	.resume
	{
		padding-left: 20px;
	}
	.pie 
	{
		font-size: 10em;
		height: 1em;
		width: 1em;
		display: inline-block;
		position: relative;
		border-radius: 100%;
		box-shadow: inset 0px 0px 0px 14px rgb(239, 239, 239);
	}
	.pie .label 
	{
		background: #34495e;
		border-radius: 50%;
		bottom: 0.4em;
		color: #ecf0f1;
		cursor: default;
		display: block;
		font-size: 0.25em;
		line-height: 2.6em;
		position: absolute;
		text-align: center;
		top: 0.4em;
		width: 100%;
	}
	.pie .label .smaller 
	{
		color: #bdc3c7;
		font-size: .45em;
		padding-bottom: 20px;
		vertical-align: super;
	}
	.pie .label 
	{
		background: none;
		color: #7f8c8d;
	}
	.pie .label .smaller
	{
		color: #bdc3c7;
	}
	.pie svg {
		position: absolute;
		top: 0;
		left: 50%;
		-webkit-transform: translateX(-50%);
		transform: translateX(-50%);
		pointer-events: none;
	}
	.pie svg path
	{
		opacity: 0;
		fill: none;
	}
	.pie svg.progress-circle path
	{
		stroke: #1E9ECD;
		stroke-width: 5;
		opacity: 1;
		-webkit-transition: stroke-dashoffset 0.3s;
		transition: stroke-dashoffset 0.3s;
	}
	.pie svg.progress-circle path
	{
		stroke-dasharray: 204;
		stroke-dashoffset: 204;
		transition: 2s;
	}
	.resume_data
	{
		display: inline-block;
	}
	.resume_data strong
	{
		display: block;
		text-align: center;
	}
</style>

<section>
	@foreach ($data as $key => $value)
		<div class="skillbar clearfix ">
			<div class="skillbar-title" ><span>{{ $key }}</span></div>
			<div class="skillbar-bar" style="width: {{ ($value['elapsed'] / $max) * 90 }}%;"></div>
			<div class="skill-bar-percent">{{ $elapse = round($value['elapsed'], 3) }}s</div>
		</div>

		<div class='resume'>
			@foreach ($value['marks'] as $mark => $time)
				<?php
					$time = round($time, 3);
					$perc = round(($time / $elapse) * 100);
					if($time < 0.999)
					{
						$time = $time * 1000;
						$type = 'ms';
					}
					else
						$type = 's';
				?>

				<div class='resume_data'>
					<strong>{{ $mark }}</strong>
					<div class="pie" data-percent='{{ $perc }}'>
						<span class="label">{{ $time }}<span class="smaller">{{ $type }}</span></span>
						<svg class="progress-circle" viewBox="0 0 70 70">
							<path d="m35,2.5c17.955803,0 32.5,14.544199 32.5,32.5c0,17.955803 -14.544197,32.5 -32.5,32.5c-17.955803,0 -32.5,-14.544197 -32.5,-32.5c0,-17.955801 14.544197,-32.5 32.5,-32.5z"/>
						</svg>
					</div>
				</div>
			@endforeach
		</div>
	@endforeach

	<script>
		$(document).ready(function() {
			$('.pie').each(function(){
				var elem = $('path', $(this))[0];
				var perc = $(this).data('percent');
				elem.style.strokeDashoffset = 204 - (2.04 * perc);
			});
		});
	</script>
</section>