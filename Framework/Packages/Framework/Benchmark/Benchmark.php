<?php

class Benchmark
{

    /////////////////////
    // Attributs
    ///////

    /**
     * Timestamp lors du démarrage du benchmark
     * @var float
     */
    protected static $start = -1;

    /**
     * Timestamp lors de l'arret du benchmark
     * @var float
     */
    protected static $stop = -1;

    /**
     * Liste des points d'arrets
     * @var array
     */
    protected static $marks = [];

    /**
     * Objet de mise en cache
     * @var Cache
     */
    protected static $cache = null;

    /////////////////////
    // Static methods
    ///////

    /**
     * Début du benchmarking
     *
     * @param mixed $time : Utilisation d'un timestamp comme base de départ
     *
     * @return mixed : False si déja démarré, timestamp sinon.
     */
    public static function start($time = false)
    {
        if (-1 !== self::$start) {
            return false;
        }

        if ($time) {
            return self::$start = $time;
        }

        return self::$start = microtime(true);
    }

    /**
     * Stop le benchmarking
     */
    public static function stop()
    {
        if (-1 === self::$stop) {
            self::$stop = microtime(true);
        }

    }

    /**
     * Reset le benchmarking
     */
    public static function reset()
    {
        self::$start = -1;
        self::$stop  = -1;
        self::$marks = [];
    }

    /**
     * Debut d'un benchmark nommé
     *
     * @param string  $name  : Nom du benchmark
     * @param boolean $start : Utilisation du temps de démarrage du benchmark
     */
    public static function mark($name, $start = false)
    {
        if (-1 === self::$start) {
            return;
        }

        if (isset(self::$marks[$name])) {
            self::$marks[$name]['suppl'] = microtime(true);
        } else {
            $data                = [];
            $data['time']        = $start ? self::$start : microtime(true);
            $data['since_start'] = -1 !== self::$start ? ($data['time'] - self::$start) : -1;
            $data['finished']    = false;
            self::$marks[$name]  = $data;
        }
    }

    /**
     * Fin d'un benchmark nommé
     *
     * @param string $name : Nom du benchmark
     */
    public static function end($name)
    {
        if (-1 === self::$start) {
            return;
        }

        if (!isset(self::$marks[$name])) {
            return;
        }

        $data = self::$marks[$name];
        if (true === $data['finished']) {
            if (isset($data['suppl'])) {
                $data['elapsed'] += microtime(true) - $data['suppl'];
                unset($data['suppl']);
            } else {
                return;
            }

        } else {
            $data['elapsed']  = microtime(true) - $data['time'];
            $data['finished'] = true;
        }

        self::$marks[$name] = $data;
    }

    /**
     * Retourne la liste des point d'arret
     * @return mixed : False si aucun point d'arret
     */
    public static function getMarks()
    {
        $data = [];
        foreach (self::$marks as $name => $value) {
            if (true === $value['finished']) {
                $value['name'] = $name;
                $data[]        = $value;
            }
        }

        return $data;
    }

    /**
     * Retourne le temps moyen entre chaque points d'arrets
     * @return float
     */
    public static function getAverage()
    {
        if ($count = count($marks = self::getMarks())) {
            $sum = 0;
            foreach ($marks as $mark) {
                $sum += $mark['elapsed'];
            }

            return $sum / $count;
        }

        return -1;
    }

    /**
     * Retourne le plus long point d'arret
     * @return mixed : False si aucun point d'arret
     */
    public static function getLongest()
    {
        if (count($marks = self::getMarks())) {
            $longest = null;
            foreach ($marks as $mark) {
                if (null === $longest || $longest['elapsed'] < $mark['elapsed']) {
                    $longest = $mark;
                }

            }

            return $longest;
        }

        return false;
    }

    /**
     * Retourne le plus court point d'arret
     * @return mixed : False si aucun point d'arret
     */
    public static function getShortest()
    {
        if (count($marks = self::getMarks())) {
            $shortest = null;
            foreach ($marks as $mark) {
                if (null === $shortest || $shortest['elapsed'] > $mark['elapsed']) {
                    $shortest = $mark;
                }

            }

            return $shortest;
        }

        return false;
    }

    /**
     * Retourne le dernier point d'arret
     * @return mixed : False si aucun point d'arret
     */
    public static function getLast()
    {
        $marks = self::getMarks();
        if (count($marks)) {
            return $marks[count($marks) - 1];
        }

        return false;
    }

    /**
     * Retourne les statistique du benchmarking
     *
     * @param boolean $stat : Retourne le timestamp de démarrage et de fin
     *
     * @return array
     */
    public static function getStats($stat = true)
    {
        if (-1 === self::$start) {
            return false;
        }

        $elapsed = ((-1 !== self::$stop) ? self::$stop : microtime(true)) - self::$start;
        $data    = [];
        if (count(self::getMarks())) {
            $data['average']  = self::getAverage();
            $data['longest']  = self::getLongest();
            $data['shortest'] = self::getShortest();
            $data['marks']    = [];
            foreach (self::getMarks() as $mark) {
                $data['marks'][$mark['name']] = $mark['elapsed'];
            }

        }

        if ($stat) {
            $data['start'] = self::$start;
            $data['stop']  = self::$stop;
        }
        $data['elapsed'] = $elapsed;

        return $data;
    }

    /**
     * Sauvegarde l'information dans un fichier
     */
    public static function save()
    {
        if (-1 === self::$start) {
            return;
        }

        $data   = self::getStats(false);
        $cache  = self::getCache();
        $result = [];
        $uri    = Application::getRequest()->route();
        if ($result = $cache->read('benchmark.data')) {
            if (isset($result[$uri])) {
                $result[$uri] = arrayAverage($result[$uri], $data);
            } else {
                $result[$uri] = $data;
            }

        } else {
            $result[$uri] = $data;
        }

        $cache->write('benchmark.data', $result);
    }

    /**
     * Récupération des valeurs de benchmark sauvegardées
     * @return array
     */
    public static function load()
    {
        $cache = self::getCache();

        return $cache->read('benchmark.data');
    }

    /**
     * Retourne l'objet de mise en cache
     * @return Cache
     */
    protected static function getCache()
    {
        if (null === self::$cache) {
            self::$cache = new Cache('benchmark', -1);
        }

        return self::$cache;
    }

}
