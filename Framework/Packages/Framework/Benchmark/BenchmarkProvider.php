<?php

class BenchmarkProvider implements Provider
{

    public static function register()
    {

        /**
         * Affichage des données mise en cache
         * lors des benchmarking
         */
        Router::respond('GET', '/benchmark', function () {
            // Empeche le benchmarking de cette page
            Benchmark::reset();

            $data = Benchmark::load();
            $max  = 0;
            foreach ($data as $key => $value) {
                if ($value['elapsed'] > $max) {
                    $max = $value['elapsed'];
                }
            }

            return View::make('@framework.benchmark/index', [
                'title' => 'Benchmarking de l\'application',
                'data'  => $data,
                'max'   => $max,
            ], 'global');
        });
    }
}