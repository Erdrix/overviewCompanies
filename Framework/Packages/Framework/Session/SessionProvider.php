<?php

class SessionProvider implements Provider
{

    /**
     * Gestion de la requête
     *
     * @return mixed
     */
    public static function register()
    {
        $request = Container::get('Request');
        Session::sessionStart($request);
    }
}