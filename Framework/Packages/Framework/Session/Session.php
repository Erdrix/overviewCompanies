<?php

class Session
{
    //////////////////////////
    // Attributs
    /////////

    private static $proxy = ['195.93.', '205.188', '198.81.', '207.200', '202.67.', '64.12.9'];

    //////////////////////////
    // Static Methods
    /////////

    /**
     * Régénere la session
     */
    public static function regenerate()
    {
        session_regenerate_id(true);
    }

    /**
     * Détruit la session en cours
     */
    public static function destroy()
    {
        $_SESSION = [];
        $params   = session_get_cookie_params();
        setcookie(session_name(), '', time() - 42000, $params['path'], $params['domain'], $params['secure'],
            $params['httponly']);

        session_destroy();
        session_start();
    }

    /**
     * Démarrage des sessions
     *
     * @param Request $request
     */
    public static function sessionStart(Request $request)
    {
        // Récupération de la configuration
        $conf = Conf::get('session');

        // Configuration du hashage
        if (strnatcmp(phpversion(), '5.2.10') >= 0) {
            ini_set('session.hash_function', 'sha512');
        } else {
            ini_set('session.hash_function', 1);
        }
        ini_set('session.entropy_file', '/dev/urandom');
        ini_set('session.hash_bits_per_character', 5);
        ini_set('session.entropy_length', 512);

        // Configuration des session
        ini_set('session.use_trans_sid', 0);
        ini_set('session.use_only_cookies', 1);
        ini_set('session.cookie_httponly', 1);
        ini_set('session.cookie_lifetime', 0);
        ini_set('session.cookie_secure', $request->secure() ? 1 : 0);
        ini_set("session.cookie_lifetime", $conf['cookie']);
        ini_set("session.gc_maxlifetime", $conf['duration']);
        ini_set("session.gc_divisor", 1);
        ini_set("session.gc_probability", 1);

        // Chemin de sauvegarde du cookie
        if ($conf['path'] !== false) {
            session_save_path(getTemporary($conf['path']));
        }

        // Choix du nom du cookie
        session_name($conf['name']);

        // Démarrage
        session_start();

        // Protection des session
        if (self::Hijacking($request)) {
            $_SESSION                          = [];
            $_SESSION['session']['ip']         = $request->ip();
            $_SESSION['session']['user-agent'] = $request->user_agent();
            self::regenerate();
        } elseif (!$request->ajax() && rand(1, 100) <= 50) {
            self::regenerate();
        }
    }

    //////////////////////////
    // Protected Methods
    /////////

    /**
     * Prévention contre le Hijacking
     *
     * @param Request $request
     *
     * @return boolean : True si valeurs non conforme
     */
    protected static function Hijacking(
        Request $request
    ) {
        if (!isset($_SESSION['session']['ip']) || !isset($_SESSION['session']['user-agent'])) {
            return true;
        }

        if (($_SESSION['session']['user-agent'] !== $request->user_agent()) && !(strpos($_SESSION['session']['user-agent'],
                    'Trident') !== false && (strpos($request->user_agent(), 'Trident') !== false))
        ) {
            return true;
        }

        $session_ip = substr($_SESSION['session']['ip'], 0, 7);
        $remote_ip  = substr($request->ip(), 0, 7);

        if ($_SESSION['session']['ip'] !== $request->ip() && !(in_array($session_ip,
                    self::$proxy) && in_array($remote_ip, self::$proxy))
        ) {
            return true;
        }

        if ($_SESSION['session']['user-agent'] !== $request->user_agent()) {
            return true;
        }

        return false;
    }
}