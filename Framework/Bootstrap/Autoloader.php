<?php

/**
 * Filtrage des extension de fichiers
 */
class ExtensionFilterIteratorDecorator extends FilterIterator
{
    private $_ext;

    public function accept()
    {
        if (substr($this->current(), -1 * strlen($this->_ext)) === $this->_ext) {
            return is_readable($this->current());
        }

        return false;
    }

    /**
     * Définis l'extension
     *
     * @param string $pExt
     */
    public function setExtension($pExt)
    {
        $this->_ext = $pExt;
    }
}

/**
 * Auto chargement des fichiers requis pour
 *    le fonctionnement de l'application
 */
class DirectoriesAutoloader
{
    private static $_instance      = false;
    private        $_canRegenerate = true;
    private        $_classes       = [];
    private        $_directories   = [];
    private        $_cachePath;

    private function __construct()
    {
    }

    /**
     * Register de l'autoloader
     *                               Utilisé lors de l'initialisation de l'objet
     *
     * @param  string $pTmpPath : Chemin utilisé pour la sauvegarde du cache
     * @param  array  $folder   : Liste des dossiers à ajouter au chemin de recherche
     *
     * @return DirectoriesAutoloader
     */
    public static function register($pTmpPath, $folder = [])
    {
        $autoloader = new DirectoriesAutoloader();
        $autoloader->setCachePath($pTmpPath);
        foreach ($folder as $name) {
            $autoloader->addDirectory($name);
        }

        spl_autoload_register([$autoloader, 'autoload']);

        return (self::$_instance = $autoloader);
    }

    /**
     * Récupération du singleton
     * @return mixed : false si pas d'instance
     */
    public static function instance()
    {
        return self::$_instance;
    }

    /**
     * Définis le chemin de sauvegarde du cache
     *
     * @param string $pTmp : Chemin vers le cache
     *
     * @throws Exception   : Fichier de cache invalide
     */
    public function setCachePath($pTmp)
    {
        $path = getTemporary($pTmp);

        if (!is_writable($path)) {
            throw new Exception('Cannot write in given CachePath [' . $path . ']');
        }

        $this->_cachePath = $path;
    }

    /**
     * Fonction utilisée lors de la résolution de chemin
     *                False       : Impossible de charger le fichier
     *                Object      : Contenu du fichier chargé
     *
     * @param  string $pClassName : Nom de la classe à charger
     *
     * @return mixed  True : Classe déja chargée
     */
    public function autoload($pClassName)
    {
        if ($this->_loadClass($pClassName)) {
            return true;
        }

        if ($this->_canRegenerate) {
            $this->_canRegenerate = false;
            $this->_includesAll();
            $this->_saveInCache();

            return $this->autoload($pClassName);
        }

        return false;
    }

    /**
     * Ajoute un répertoire a la liste de ceux à autoloader
     *
     * @param  string  $pDirectory : Dossier à ajouter
     * @param  boolean $pRecursive : Recherche dans les sous-dossier (default: true)
     *
     * @throws Exception
     * @return Autoloader
     */
    public function addDirectory($pDirectory, $pRecursive = true)
    {
        if (!is_readable($pDirectory)) {
            throw new Exception('Cannot read from [' . $pDirectory . ']');
        }

        $this->_directories[$pDirectory] = $pRecursive ? true : false;

        return $this;
    }

    /**
     * Recherche de toutes les classes dans les répertoires donnés
     */
    private function _includesAll()
    {
        foreach ($this->_directories as $directory => $recursive) {
            $directories = new AppendIterator();

            if ($recursive) {
                $directories->append(new RecursiveIteratorIterator(new RecursiveDirectoryIterator($directory)));
            } else {
                $directories->append(new DirectoryIterator($directory));
            }

            $files = new ExtensionFilterIteratorDecorator($directories);
            $files->setExtension('.php');

            foreach ($files as $file) {
                $classes = $this->_extractClasses((string)$file);
                foreach ($classes as $className => $fileName) {
                    $this->_classes[strtolower($className)] = $fileName;
                }

            }
        }
    }

    /**
     * Extraction des classes & interfaces d'un fichier
     *
     * @param  string $pFileName : Nom du fichier de lecture
     *
     * @return array
     */
    private function _extractClasses($pFileName)
    {
        $toReturn = [];
        $tokens   = token_get_all(file_get_contents($pFileName, false));
        $tokens   = array_filter($tokens, 'is_array');

        $classHunt = false;
        foreach ($tokens as $token) {
            if (T_INTERFACE === $token[0] || T_CLASS === $token[0]) {
                $classHunt = true;
                continue;
            }

            if ($classHunt && T_STRING === $token[0]) {
                $toReturn[$token[1]] = $pFileName;
                $classHunt           = false;
            }
        }

        return $toReturn;
    }

    /**
     * Sauvegarde la liste des classes dans le fichier de cache
     */
    private function _saveIncache()
    {
        $file   = $this->_cachePath . 'autoloader.php';
        $toSave = '<?php $classes = ' . var_export($this->_classes, true) . '; ?>';
        file_put_contents($file, $toSave);
        chmod($file, octdec('0777'));
    }

    /**
     * Charger une classe
     *
     * @param string $pClassName : Nom de la classe à charger
     *
     * @return boolean           : False si impossible de charger la classe
     */
    private function _loadClass($pClassName)
    {
        $className = strtolower($pClassName);
        if (count($this->_classes) === 0) {
            if (is_readable($this->_cachePath . 'autoloader.php')) {
                require $this->_cachePath . 'autoloader.php';
                $this->_classes = $classes;
            }
        }

        if (isset($this->_classes[$className])) {
            require_once $this->_classes[$className];

            return true;
        }

        return false;
    }

}
