<?php

/**
 * Chargement des fichiers
 *   autloader   : Résolution des chemin vers les classes
 *   helper      : Utilitaires du framework
 */
require CORE . 'Bootstrap' . DS . 'Autoloader.php';
require CORE . 'Core' . DS . 'Helper' . DS . 'Utils.php';

/**
 * Préparation de l'autoloader (résolution des classes)
 *   core        : Fichiers coeurs du framework
 *   library     : Librairies utilitaires
 *   vendors     : Livrairies externes
 *   application : Site web
 *
 * @link http://php.net/manual/fr/language.oop5.autoload.php
 */
DirectoriesAutoloader::register(
    'loader', [
        CORE . 'Core',
        CORE . 'Packages',
        APP,
    ]
);

/**
 * Lectures des differents fichiers de configuration
 *   se trouvant dans le dossier "system/config"
 */
foreach (glob(CORE . 'Config' . DS . '*.php') as $filename) {
    Conf::push(require($filename));
}

/**
 * Redirection des urls si l'url rewriting est activé
 *   Routing : ?url=file/route  =>  ?route=file/route
 *   Code    : 301 (Moved Permanently)
 */
if (Conf::get('global.url_rewrite') && isset($_GET['url'])) {
    if (isset($_SERVER['REDIRECT_URL'])) {
        $url = $_GET['url'];
        unset($_GET['route'], $_GET['url']);
        URL::location($url . (!empty($_GET) ? '?' : '') . http_build_query($_GET), 0, 307);
    } else {
        $_GET['route'] = $_GET['url'];
        unset($_GET['url']);
    }
}

/**
 * Configuration de php suivant le fichier de configuration
 *   error_reporting : Gestion de la detection des erreurs
 *   default_charset : Encodage des page web (utf8, ...)
 */
error_reporting(Conf::get('global.debug_level'));
ini_set('default_charset', Conf::get('global.charset'));

/**
 * Lancement de l'application coeur du framework
 *  Gestion de la requête, des routes, ...
 */
new Application();
