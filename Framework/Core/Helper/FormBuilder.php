<?php

class FormBuilder
{

    //////////////////////////
    // Attributs
    /////////

    /**
     * Données du formulaire
     * @var array
     */
    private $data;

    /**
     * Balise d'encadrement des elements
     * @var string
     */
    private $surround;

    //////////////////////////
    // Constructeur
    /////////

    /**
     * @param mixed  $data     : Données du formulaire
     * @param string $surround : Balise d'encadrement des elements
     */
    public function __construct($data = [], $surround = 'p')
    {
        $this->data     = $data;
        $this->surround = $surround;
    }

    //////////////////////////
    // Public Methods
    /////////

    /**
     * Création d'un input
     *
     * @param string $type  : Type de champ à créer
     * @param string $name  : Nom du champ
     * @param mixed  $label : Nom du label
     *
     * @return string
     */
    public function input($type, $name, $label = false)
    {
        $html = $this->labelize($label, $name);
        $html .= ('<input type="' . $type . '" class="former underligne" name="' . $name . '" value="' .
                  $this->getValue($name) . '" ' .
                  ($label !== false ? 'id="' . $name . '"' : null) . '/>');

        return $this->surround($html);
    }

    /**
     * Création d'un champ de type : text
     *
     * @param string $name  : Nom du champ
     * @param mixed  $label : Nom du label
     *
     * @return string
     */
    public function text($name, $label = false)
    {
        return $this->input('text', $name, $label);
    }

    /**
     * Création d'un champ de type : password
     *
     * @param string $name  : Nom du champ
     * @param mixed  $label : Nom du label
     *
     * @return string
     */
    public function password($name, $label = false)
    {
        return $this->input('password', $name, $label);
    }

    /**
     * Création d'un champ de type : hidden
     *  Le champ n'est pas crée si aucune valeur associé
     *
     * @param string $name : Nom du champ
     *
     * @return string
     */
    public function hidden($name)
    {
        if ($this->getValue($name) !== null) {
            return $this->input('hidden', $name);
        }

        return null;
    }

    /**
     * Création d'un textarea
     *
     * @param string $name  : Nom du champ
     * @param mixed  $label : Nom du label
     *
     * @return string
     */
    public function textarea($name, $label = false)
    {
        $html = $this->labelize($label, $name);
        $html .= ('<textarea name="' . $name . '" ' . ($label ? 'id="' . $name . '"' : null) . '>' .
                  $this->getValue($name) . '</textarea>');

        return $this->surround($html);
    }

    /**
     * Création d'un input de type select
     *
     * @param string $name     : Nom de l'input
     * @param array  $option   : Options de l'input
     * @param mixed  $label    : Nom du label
     * @param mixed  $elem     : Tableau de deux valeur contenant en index 0 la valeur de l'option
     *                         et en index 1 le nom d'affichage
     * @param mixed  $default  : Ajoute une valeur par défaut négative
     *
     * @return string
     */
    public function select($name, array $option = [], $label = false, $elem = false, $default = false)
    {
        $html = $this->labelize($label, $name);
        $html .= '<select name="' . $name . '" ' . ($label ? 'id="' . $name . '"' : null) . '>';

        if ($default !== false) {
            $html .= '<option value="-1">' . $default . '</option>';
        }

        $selected = $this->getValue($name);
        if ($selected === null) {
            $select = false;
        }

        if ($elem == false) {
            foreach ($option as $key) {
                $html .= '<option value="' . $key . '" ' . ($key === $selected ? 'selected' : '') . '>' . $key .
                         '</option>';
            }
        } else {
            $value = array_shift($elem);
            $visu  = empty($elem) ? [$value] : $elem;
            foreach ($option as $key) {
                $html .= '<option value="' . $key[$value] . '" ' . ($key[$value] === $selected ? 'selected' : '') . '>';
                foreach ($visu as $elem) {
                    $html .= $key[$elem] . ' ';
                }
                $html .= '</option>';
            }
        }
        $html .= '</select>';

        return $this->surround($html);
    }

    /**
     * Création d'un input de type radio
     *
     * @param string $name   : Nom de l'input
     * @param array  $option : Liste des options stocké dans un objet [value => label(, value => label...)]
     * @param mixed  $label  : Label de l'input
     *
     * @return string
     */
    public function radio($name, array $option = [], $label = false)
    {
        $html           = $this->labelize($label, $name);
        $selected       = $this->getValue($name);
        $surround       = $this->surround;
        $this->surround = false;

        foreach ($option as $value => $visu) {
            $html .=
                '<label> ' . $visu . ' <input type="radio" id="' . $name . $value . '" value="' . $value . '" name="' .
                $name . '" ' . ($selected == $value ? 'checked' : '') . ' /></label>';
        }

        $this->surround = $surround;

        return $this->surround($html);
    }

    /**
     * Création d'un bouton d'envois de formulaire
     *
     * @param mixed $label : Nom du bouton
     *
     * @return string
     */
    public function submit($label = false)
    {
        return $this->surround('<input type="submit" value="' . ($label ? $label : 'Envoyer') . '" />');
    }

    //////////////////////////
    // Private Methods
    /////////

    /**
     * Encadrement d'un element par la balise : surround
     *
     * @param string $elem : Element à encadrer
     *
     * @return string
     */
    private function surround($elem)
    {
        if ($this->surround !== false) {
            return '<' . $this->surround . '>' . $elem . '</' . $this->surround . '>';
        }

        return $elem;
    }

    /**
     * Retourne la valeur du champ si présent dans les données du formulaire
     *
     * @param string $name : Nom du champ
     *
     * @return string
     */
    private function getValue($name)
    {
        if (strpos($name, '[') !== false) {
            $name = str_replace(']', '', $name);
            $name = explode('[', $name);

            $data = $this->data;
            foreach ($name as $part) {
                if (isset($data[$part])) {
                    $data = $data[$part];
                } else {
                    return null;
                }
            }

            return $data;
        }

        return isset($this->data[$name]) ? escape($this->data[$name]) : null;
    }

    /**
     * Création d'un label pour l'objet
     *
     * @param mixed  $label : Nom du label
     * @param string $name  : Id du label
     *
     * @return string
     */
    private function labelize($label, $name)
    {
        if ($label !== false) {
            return '<label for="' . $name . '">' . $label . '</label>';
        }

        return '';
    }
}