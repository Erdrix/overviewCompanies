<?php

class CSRFToken
{

    //////////////////////////
    // Attributs
    /////////

    /**
     * Nom de la clée
     * @var string
     */
    private static $token_name = 'CSRFToken';

    //////////////////////////
    // Static Methods
    /////////

    /**
     * Génere un nouveau token CSRF
     *
     * @param int $size : Taille de la clée random
     */
    public static function generate($size = 32)
    {
        $_SESSION[self::$token_name] = base64_encode(openssl_random_pseudo_bytes($size));
    }

    /**
     * Détruit le token CSRF précedement généré
     */
    public static function destroy()
    {
        if (!isset($_SESSION[self::$token_name])) {
            unset($_SESSION[self::$token_name]);
        }
    }

    /**
     * Retourne le token utilisateur
     *
     * @return string
     */
    public static function token()
    {
        if (isset($_SESSION[self::$token_name])) {
            return $_SESSION[self::$token_name];
        }

        return '';
    }

    /**
     * Retourne le champs formulaire csrf
     *
     * @return string
     */
    public static function input()
    {
        return '<input type="hidden" id="CSRFToken" name="' . self::$token_name . '" value="' . self::token() . '" />';
    }

    /**
     * Vérifie la clée envoyé via post
     *
     * @return boolean
     */
    public static function check()
    {
        if (!isset($_POST[self::$token_name])) {
            return false;
        }

        $token = $_POST[self::$token_name];
        unset($_POST[self::$token_name]);

        return $token === self::token();
    }
}