<?php

class Hash
{

    /**
     * Crée un nouveau hash à partir d'un string
     *
     * @param string $string : Password, nom, ...
     * @param int    $cost   : Longueur du sel
     *
     * @return string : Hash du string
     */
    public static function create($string, $cost = 10)
    {
        $td   = mcrypt_module_open(MCRYPT_RIJNDAEL_256, "", MCRYPT_MODE_CBC, "");
        $salt = strtr(base64_encode(mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_DEV_URANDOM | MCRYPT_RAND)),
            '+', '.');
        $salt = sprintf("$2a$%02d$", $cost) . $salt;

        return crypt($string, $salt);
    }

    /**
     * Retourne si le string correspond au hash
     *
     * @param  string $string : String à vérifier
     * @param  string $hash   : Hash à vérifier
     *
     * @return boolean
     */
    public static function check($string, $hash)
    {
        return self::hash_comp($hash, crypt($string, $hash));
    }

    /**
     * Compare la validité de deux hash
     *
     * @param string $hash    : Le hash à comparer
     * @param string $to_comp : La chaine à comparer au hash initial
     *
     * @return boolean True si les deux chaines sont égales
     */
    public static function hash_comp($hash, $to_comp)
    {
        if (strlen($hash) != strlen($to_comp)) {
            return false;
        } else {
            $res = $hash ^ $to_comp;
            $ret = 0;
            for ($i = strlen($res) - 1; $i >= 0; $i--) {
                $ret |= ord($res[$i]);
            }

            return !$ret;
        }
    }

}
