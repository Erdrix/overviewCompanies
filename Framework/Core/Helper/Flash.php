<?php

class Flash
{

    //////////////////////////
    // Static Methods
    /////////

    /**
     * Affiche un message flash
     *
     * @param string $name : Nom du message
     */
    public static function get($name)
    {
        $tag = false;
        if (isset($_SESSION['flash']['new'][$name])) {
            $tag = 'new';
        } elseif (isset($_SESSION['flash']['old'][$name])) {
            $tag = 'old';
        }

        if ($tag !== false) {
            echo '<div class="alert alert-' . $_SESSION['flash'][$tag][$name . '_class'] . '">';
            echo $_SESSION['flash'][$tag][$name];
            echo '</div>';

            unset($_SESSION['flash'][$tag][$name], $_SESSION['flash'][$tag][$name . '_class']);
        }
    }

    /**
     * Définis un message flash
     *
     * @param string $name    : Nom du message
     * @param string $message : Valeur du message
     * @param string $class   : Classe bootstrap utilisé lors de l'affichage
     */
    public static function set($name, $message, $class = 'info')
    {
        $_SESSION['flash']['new'][$name]            = $message;
        $_SESSION['flash']['new'][$name . '_class'] = $class;
    }

    //////////////////////////
    // Fin de session
    /////////
    /**
     * Utilisé pour les session Flahs
     */
    public static function endSession()
    {
        if (isset($_SESSION['flash']['new'])) {
            $_SESSION['flash']['old'] = $_SESSION['flash']['new'];
            unset($_SESSION['flash']['new']);
        } elseif (isset($_SESSION['flash']['old'])) {
            unset($_SESSION['flash']['old']);
        }
    }
}