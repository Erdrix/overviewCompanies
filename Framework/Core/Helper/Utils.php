<?php

// Permission des dossier
$_folder = null;

/**
 * Affiche tous les elements d'un tableau
 *
 * @param mixed $data : Donnée(s) à afficher
 */
function dump($data)
{
    echo '<pre>';
    $type = gettype($data);
    if ('object' === $type && !($data instanceof Closure)) {
        $type = get_class($data);
        $data = get_object_vars($data);
    }

    echo '<span style="color:#037dee">' . $type . '</span>';
    if (is_array($data)) {
        echo '(' . sizeof($data) . ') {';
        printArray($data, 1);
        echo '<br>}';
    } elseif ($data instanceof Closure) {
        printFunction($data);
    } else {
        $length = strlen($data);
        if (is_bool($data)) {
            $data = $data ? 'true' : 'false';
        }
        echo '(' . $length . ') "<span style="color:#92b938">' . escape($data) . '</span>"';
    }
    echo '</pre>';
}

/**
 * Parcours récursif d'un tableau
 *
 * @param array   $array  : Tableau à parcourir
 * @param integer $indent : Nombre d'indentation du texte
 */
function printArray($array, $indent)
{
    $space = indent($indent);

    if (empty($array)) {
        echo '<br>' . $space . '<i style="color:rgba(149, 149, 145, 0.64)">empty</i>';
    } else {
        foreach ($array as $key => $value) {
            $type = gettype($value);
            if ('object' === $type && !($value instanceof Closure)) {
                $type  = get_class($value);
                $value = get_object_vars($value);
            }

            echo '<br>' . $space;
            echo '["<i style="color:red">' . escape($key) . '</i>"] => <span style="color:#037dee">' . $type .
                 '</span>';

            if (is_array($value)) {
                echo '(' . sizeof($value) . ') {';
                printArray($value, $indent + 1);
                echo '<br>' . $space . '}';
            } elseif ($value instanceof Closure) {
                printFunction($value);
            } else {
                $length = strlen($value);
                if (is_bool($value)) {
                    $value = $value ? 'true' : 'false';
                }
                echo '(' . $length . ') "<span style="color:#92b938">' . escape($value) . '</span>"';
            }
        }
    }
}

/**
 * Affiche le constructeur de la fonction
 *
 * @param Callable $closure : Fonction à afficher
 */
function printFunction(Callable $closure)
{
    $reflection = new ReflectionFunction($closure);
    $arguments  = $reflection->getParameters();
    $list       = [];
    foreach ($arguments as $param) {
        $list[] = $param->getName();
    }

    echo '(Closure) ';
    if (!empty($list)) {
        echo '"<span style="color:orange">$' . implode(', $', $list) . '</span>"';
    }
}

/**
 * Retourne une chaîne de caractère contenant des espaces
 *
 * @param  integer $indent : Nombre d'indentation du texte
 *
 * @return string
 */
function indent($indent)
{
    $space = '';
    for ($i = 0; $i < $indent * 4; $i++) {
        $space = $space . '&nbsp;';
    }

    return $space;
}

/**
 * Protege contre des inclusions de données
 *
 * @param string $string : Valeur à échapper
 *
 * @return string
 */
function escape($string)
{
    return htmlspecialchars($string, ENT_QUOTES, 'UTF-8');
}

/**
 * Protege un tableau contre des inclusions de données
 *
 * @param mixed $object
 *
 * @return array
 */
function array_escape($object)
{
    if (is_array($object)) {
        $temp = [];
        foreach ($object as $key => $value) {
            $type = gettype($value);
            if (is_array($value) || $type === 'object') {
                $temp[$key] = array_escape($value);
            } else {
                $temp[$key] = escape($value);
            }
        }

        return $temp;
    } else {
        $type = gettype($object);
        if ($type === 'object') {
            $list = get_object_vars($object);
            foreach ($list as $key => $value) {
                $object->$key = escape($value);
            }

            return $object;
        } else {
            return escape($object);
        }
    }
}

/**
 * Retourne un tableau contenant la moyenne des valeurs présent dans les deux tableau
 *
 * @param  array $data : Tableau source
 * @param  array $new  : Tableau contant les nouvelles valeurs
 *
 * @return array
 */
function arrayAverage($data, $new)
{
    $temp = [];
    foreach ($data as $key => $value) {
        if (isset($new[$key])) {
            if (is_array($data[$key])) {
                if (is_array($new[$key])) {
                    $temp[$key] = arrayAverage($data[$key], $new[$key]);
                } else {
                    $temp[$key] = $data[$key];
                }

            } else {
                $temp[$key] = ($data[$key] + $new[$key]) / 2;
            }

        } else {
            $temp[$key] = $value;
        }

    }

    return $temp;
}

/**
 * Débug la variable et stop le script
 *
 * @param mixed $data ; Variable à débug
 */
function dd($data)
{
    echo '<div style="position: absolute; z-index:1000;">';
    dump($data);
    echo '</div>';
    die();
}

/**
 * Retourne le dossier temporaire de l'application
 *
 * @param  boolean $folder : Nom du sous-dossier
 *
 * @return string  : Chemin absolue vers le dossier
 */
function getTemporary($folder = false)
{
    $path = CORE . 'Tmp' . DS;
    if ($folder) {
        $path .= $folder . DS;
    }

    if (class_exists('Conf')) {
        $folder = Conf::get('global.permission.folders');
    } else {
        $folder = 774;
    }

    if (!is_dir($path)) {
        mkdir($path, octdec('0' . $folder), true);
    }

    return $path;
}

/**
 * Supprime les antislashes dans un tableau
 *
 * @param  array $value : Tableau à parcourir
 *
 * @return array
 */
function stripSlashesDeep($value)
{
    $value = is_array($value) ? array_map([$this, 'strip_slashes_deep'], $value) : stripslashes($value);

    return $value;
}

/**
 * Flatten un tableau de données
 *
 * @param array   $array : Tableau de valeurs
 * @param integer $level : Profondeur d'analyse
 *
 * @return array
 */
function flatten(array $array, $level = 0)
{
    $result = [];
    foreach ($array as $i => $v) {
        if (0 <= $level && is_array($v)) {
            $v      = flatten($v, $level > 1 ? $level - 1 : 0 - $level);
            $result = array_merge($result, $v);
        } else {
            $result[] = $v;
        }
    }

    return $result;
}

/**
 * Flatten un tableau associatif de données
 *
 * @param array  $array : Tableau de valeurs
 * @param string $glue  : Jointure des clée
 *
 * @return array
 */
function flattenAssoc(array $array, $glue = '.')
{
    $result = [];
    foreach ($array as $key => $value) {
        if (is_array($value)) {
            $temp = flattenAssoc($value);
            foreach ($temp as $temp_key => $temp_value) {
                $result[$key . $glue . $temp_key] = $temp_value;
            }
        } else {
            $result[$key] = $value;
        }
    }

    return $result;
}
