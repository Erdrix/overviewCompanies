<?php

class URL
{

    /**
     * Retourne l'url vers la route
     *
     * @param  string $name : Nom de la route
     *
     * @return string
     */
    public static function route($name)
    {
        // Soon
    }

    /**
     * Retourne la mise en forme du header pour un redirection
     *
     * @param string  $url   : url de la page
     * @param integer $timer : Temps avant redirection
     * @param integer $code  : Code de redirection
     */
    public static function location($url, $timer = 0, $code = 302)
    {
        $absolute = strpos($url, '://');
        header(($timer > 0 ? 'Refresh:' . $timer . ';' : '') . (0 != $timer ? 'url=' : 'Location: ') .
               ($absolute ? '' : URL . '/') . $url,
            true, $code);

        if ($timer === 0) {
            die();
        }
    }
}
