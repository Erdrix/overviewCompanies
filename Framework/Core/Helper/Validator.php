<?php

class Validator
{

    //////////////////////////
    // Attributs
    /////////

    /**
     * Base de donnée
     * @var Database
     */
    private $db;

    /**
     * Liste des erreurs
     * @var array
     */
    private $errors = [];

    //////////////////////////
    // Constructeur
    /////////

    public function __construct(Database $db)
    {
        $this->db = $db;
    }

    //////////////////////////
    // Public Methods
    /////////

    /**
     * Vérifie la validité des données suivant les rêgles de vérification
     *
     * @param array $data  : Liste des données à vérifier
     * @param array $verif : Rêgles de vérification
     *
     * @return boolean
     */
    public function check(&$data, $verif = [])
    {
        foreach ($verif as $item => $rules) {
            foreach ($rules as $rule => $rule_value) {
                $value = trim(escape($data[$item]));

                if ($rule === 'required' && empty($value)) {
                    $this->addError("{$item} est requis.");
                } elseif ($rule === 'default' && empty($value)) {
                    $data[$item] = $rule_value;
                } else {
                    switch ($rule) {
                        case 'min':
                            if (strlen($value) < $rule_value) {
                                $this->addError("Le champ '{$item}' doit faire plus de {$rule_value} caracters.");
                            }
                            break;
                        case 'max':
                            if (strlen($value) > $rule_value) {
                                $this->addError("Le champ '{$item}' doit faire moins de {$rule_value} caracters.");
                            }
                            break;
                        case 'matche':
                            if ($value !== $data[$rule_value]) {
                                $this->addError("La valeur du champ '{$item}' doit être identique au champ '{$rule_value}'.");
                            }
                            break;
                        case 'unique':
                            $rule_value = explode('.', $rule_value);
                            if ($this->db->isexist($rule_value[0], [$rule_value[1] => $value])) {
                                $this->addError("{$item} '{$value}' existe déja en base de donnée.");
                            }
                            break;
                    }
                }
            }
        }
    }

    /**
     * Retourne la liste des erreurs
     *
     * @return array | boolean : False si aucune erreurs
     */
    public function errors()
    {
        if (empty($this->errors)) {
            return false;
        }

        return $this->errors;
    }

    //////////////////////////
    // Private Methods
    /////////

    /**
     * Ajoute une erreur à la liste
     *
     * @param string $error
     */
    private function addError($error)
    {
        $this->errors[] = $error;
    }

}