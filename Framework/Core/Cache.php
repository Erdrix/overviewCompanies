<?php

class Cache
{

    //////////////////////////
    // Attributs
    /////////

    public static $array;

    /**
     * Buffer du cache
     * @var mixed
     */
    private       $buffer;

    /**
     * Dossier de sauvegarde des fichiers caches
     * @var string
     */
    private       $dirname;

    /**
     * Durée du cache en minutes
     * @var int
     */
    private       $duration;

    /**
     * Permissions lors de la création de fichiers
     * @var int
     */
    private       $permission;

    //////////////////////////
    // Constructeur
    /////////

    /**
     * Gestionnaire de mise en cache de contenu
     *
     * @param string  $direname : Dossier de sauvegarde du cache
     * @param integer $duration : Temps de la mise en cache en minute
     */
    public function __construct($direname, $duration)
    {
        $this->dirname  = getTemporary('cache' . DS . $direname);
        $this->duration = $duration;

        $conf             = Conf::get('global');
        $this->permission = $conf['permission']['files'];
    }

    //////////////////////////
    // Public Methods
    /////////

    /**
     * Recherche si le fichier est déja présent en cache
     *
     * @param  string $filename : Nom du fichier à rechercher
     *
     * @return boolean : True si le fichier existe déja
     */
    public function inCache($filename)
    {
        $file = $this->dirname . '/' . $filename;
        if (!file_exists($file)) {
            return false;
        }

        if (-1 !== $this->duration) {
            $life = (time() - filemtime($file)) / 60;
            if ($life > $this->duration) {
                return false;
            }

        }

        return true;
    }

    /**
     * Suppression d'un fichier de cache
     *
     * @param string $filename : Nom du fichier de sauvegarde
     */
    public function delete($filename)
    {
        $file = $this->dirname . '/' . $filename;
        if (file_exists($file)) {
            unlink($file);
        }

    }

    /**
     * Suppression de tous les fichiers de cache
     */
    public function clear()
    {
        $files = glob($this->dirname . '/*');
        foreach ($files as $file) {
            unlink($file);
        }

    }

    /**
     * Inclusion de fichier avec mise en cache
     *
     * @param  string $file     : Chemin vers le fichier
     * @param  mixed  $filename : Nom du fichier de sauvegarde
     *
     * @return boolean : True si aucune erreur
     */
    public function inc($file, $filename = false)
    {
        if (!$filename) {
            $filename = basename($file);
        }

        if ($content = $this->read($filename)) {
            echo $content;

            return true;
        }

        ob_start();
        require $file;
        $content = ob_get_clean();
        $this->write($filename, $content);
        echo $content;

        return true;
    }

    /**
     * Récupération du cache
     *
     * @param  string  $filename  : Nom du fichier de sauvegarde
     * @param  boolean $serialize : Unserialize le contenue lors de la lecture
     *
     * @return mixed   : False si erreur lors de la lecture
     */
    public function read($filename, $serialize = true)
    {
        $file = $this->dirname . '/' . $filename;
        if (!file_exists($file)) {
            return false;
        }

        if (-1 !== $this->duration) {
            $life = (time() - filemtime($file)) / 60;
            if ($life > $this->duration) {
                return false;
            }

        }

        if (!$serialize) {
            return file_get_contents($file);
        }

        return unserialize(file_get_contents($file));
    }

    /**
     * Ecriture de cache
     *
     * @param  string  $filename  : Nom du fichier de sauvegarde
     * @param  string  $content   : Contenu à mettre en cache
     * @param  boolean $serialize : Serialize le contenue lors de l'écriture
     *
     * @return boolean : False si l'écriture échoue
     */
    public function write($filename, $content, $serialize = true)
    {
        if ($serialize) {
            $content = serialize($content);
        }

        if (!file_put_contents($this->dirname . '/' . $filename, $content)) {
            return false;
        }

        chmod($this->dirname . '/' . $filename, octdec('0' . $this->permission));

        return true;
    }

    /**
     * Démarre la mise en cache
     *
     * @param  string $filename : Nom du fichier de sauvegarde
     *
     * @return boolean : True si déja en cache
     */
    public function start($filename)
    {
        if ($content = $this->read($filename)) {
            echo $content;
            $this->buffer = false;

            return true;
        }

        ob_start();
        $this->buffer = $filename;

        return false;
    }

    /**
     * Termine la mise en cache
     * @return boolean : False si aucune mise en cache démarré.
     */
    public function end()
    {
        if (!$this->buffer) {
            return false;
        }

        $content = ob_get_clean();
        $this->write($this->buffer, $content);
        echo $content;

        return true;
    }

}
