<?php

class Redirect implements Response
{

    //////////////////////////
    // Attributs
    /////////

    /**
     * Url de la redirection
     * @var string
     */
    private $url;

    /**
     * Code de redirection
     * @var int
     */
    private $code;

    //////////////////////////
    // Constructeur
    /////////

    /**
     * Constructeur de l'objet
     *
     * @param string  $url  : Lien de la page demandée
     * @param integer $code : Code de redirection, si non renseigné : 302 (redirection temporaire)
     */
    public function __construct($url, $code)
    {
        $this->url  = $url;
        $this->code = $code;
    }

    /////////////////////
    // Static methods
    ///////

    /**
     * Création d'une redirection simple
     *
     * @param  string  $url  : Url de la page
     * @param  integer $code : Code de redirection
     *
     * @return Redirect
     */
    public static function make($url = '', $code = 302)
    {
        return new self('?url=' . $url, $code);
    }

    /**
     * Renvois vers la page précedente
     *
     * @return Redirect
     */
    public static function back()
    {
        return new self($_SERVER['HTTP_REFERER'], 302);
    }

    //////////////////////////
    // Public Methods
    /////////

    /**
     * Redirige l'utilisateur
     */
    public function call()
    {
        URL::location($this->url, 0, $this->code);
    }

    /**
     * Retourne l'url demandée par la redirection
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Retourne le code demandé par la redirection
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }
}
