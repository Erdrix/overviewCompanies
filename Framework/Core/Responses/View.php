<?php

class View implements Response
{

    //////////////////////////
    // Attributs
    /////////

    /**
     * Parametres de la vue
     * @var array
     */
    private $parametres;

    /**
     * Chemin vers la vue
     * @var string
     */
    private $path;

    /**
     * Template de la vue
     * @var string
     */
    private $template;

    //////////////////////////
    // Constructeur
    /////////

    /**
     * View :
     *  Affiche une page à l'utilisateur
     *
     * @param string $view       : Nom d'une vue se trouvant dans le dossier Application/Views/{name}
     *                           Les '.' sont transformés en séparateur de dossier '/'
     *                           Les '@' signale que la vue se trouve dans un framework
     * @param array  $parametres : Variables envoyées à la vue (sous forme de tableau)
     * @param mixed  $template   : Utilisation d'un template si renseigné, les templates se trouvent
     *                           dans le dossier Application/Views/__Template/{name}
     */
    public function __construct($view, $parametres = [], $template = false)
    {
        $this->setView($view);
        $this->parametres = $parametres;
        $this->template   = $template;
    }

    //////////////////////////
    // Static Methods
    /////////

    /**
     * Création d'une vue
     *
     * @param  string $path       : Chemin vers la vue
     * @param  array  $parametres : Liste de parametres
     * @param  mixed  $template   : Template de la vue
     *
     * @return View
     */
    public static function make($path, array $parametres = [], $template = false)
    {
        if (Router::getConfig('variables')) {
            $parametres = array_merge(Router::getConfig('variables'), $parametres);
        }

        if (false === $template) {
            $template = Router::getConfig('template');
        }

        return new self($path, $parametres, $template);
    }

    /////////////////////
    // Public Methods
    ///////

    /**
     * Retourne le chemin de la vue
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Retourne les parametres de la vue
     * @return array
     */
    public function getParametres()
    {
        return $this->parametres;
    }

    /**
     * Retourne le template utilisé
     * @return string
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * Définie la vue
     *
     * @param string $view
     */
    public function setView($view)
    {

        if (strpos($view, '@') === false) {
            $view       = str_replace('.', DS, $view);
            $this->path = APP . 'views' . DS . $view . '.php';
        } else {
            $view       = str_replace('@', '', $view);
            $module     = explode('/', $view, 2);
            $path       = str_replace(' ', DS, ucwords(str_replace('.', ' ', $module[0])));
            $vue        = str_replace('/', DS, $module[1]);
            $this->path = CORE . 'Packages' . DS . $path . DS . 'views' . DS . $vue . '.php';
        }
    }

    /**
     * Définie les parametres de la vue
     *
     * @param array $parametres
     */
    public function setParametres($parametres)
    {
        $this->parametres = $parametres;
    }

    /**
     * Définie le template de la vue
     *
     * @param string $template
     */
    public function setTemplate($template)
    {
        $this->template = $template;
    }

    /**
     * Affiche la vue
     */
    public function call()
    {
        $data  = ['path' => $this->path, 'template' => $this->template, 'params' => $this->parametres];
        $event = Dispatcher::call('view.render', $data);

        if (!$event->isCancelled()) {
            extract($this->parametres);
            if (!is_file($this->path)) {
                trigger_error('Impossible de charger la vue ' . $event->path);
            }

            ob_start();
            require $this->path;
            $content_for_template = ob_get_clean();

            if ($this->template) {
                require APP . 'views/__template/' . $this->template . '.php';
            } else {
                echo $content_for_template;
            }

        } else {
            echo $event->content;
        }

    }
}
