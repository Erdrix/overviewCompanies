<?php

class Event
{

    //////////////////////////
    // Attributs
    /////////

    /**
     * L'event est cancelled
     * @var boolean
     */
    public $cancelled;

    //////////////////////////
    // Constructeur
    /////////

    /**
     * Constructeur de l'objet
     *
     * @param array $params : Parametres de l'event
     */
    public function __construct($params)
    {
        // Lecture des arguments
        foreach ($params as $key => $value) {
            $this->$key = $value;
        }

        $this->cancelled = false;
    }

    //////////////////////////
    // Public Methods
    /////////

    /**
     * Retourne vrai si l'event est cancelled
     * @return boolean
     */
    public function isCancelled()
    {
        return $this->cancelled;
    }

}
