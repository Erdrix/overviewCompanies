<?php

class Dispatcher
{

    /////////////////////
    // Attributs
    ///////

    /**
     * Liste des fonctions a vérifier lors de l'appel d'un event
     * @var array
     */
    private static $listeners = [];

    /////////////////////
    // Static methods
    ///////

    /**
     * Register un callback pour une action
     *
     * @param string   $name     : Nom de l'action
     * @param Callable $listener : Fonction à utiliser lors de l'appelle
     * @param integer  $priority : Priorité de l'event
     */
    public static function listen($name, Callable $listener, $priority = 0)
    {
        if (!isset(self::$listeners[$name])) {
            self::$listeners[$name] = [];
        }

        self::$listeners[$name][$priority][] = &$listener;
    }

    /**
     * Appelle une action précedement enregistré
     *
     * @param  string $name   : Nom de l'action
     * @param  array  $params : Paramètres transférés
     *
     * @return Event  : Objet event de retour
     */
    public static function call($name, $params = [])
    {
        $event = new Event($params);
        if (!isset(self::$listeners[$name])) {
            return $event;
        }

        foreach (self::$listeners[$name] as $priority) {
            foreach ($priority as $callback) {
                $temp = $callback($event);
                if (!null === $temp && $temp instanceof Event) {
                    $event = $temp;
                }

            }
        }

        return $event;
    }
}
