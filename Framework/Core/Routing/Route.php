<?php

class Route
{

    //////////////////////////
    // Attributs
    /////////

    /**
     * Closure de la réponse
     * @var Closure
     */
    private $function;

    /**
     * Parametres de l'uri
     * @var array
     */
    private $params;

    //////////////////////////
    // Constructeur
    /////////

    /**
     * Route retourné par le routeur lors de correspondance
     *
     * @param callable $function : Fonction de la route
     * @param          $route    : Chemin de la route
     * @param array    $params   : Parametres de la route
     */
    public function __construct(Closure $function, $route, array $params = [])
    {
        $this->function = &$function;
        $this->route    = $route;
        $this->params   = $params;
    }

    //////////////////////////
    // Public Methods
    /////////

    /**
     * Retourne le nom de la route utilisée
     * @return string
     */
    public function getName()
    {
        return $this->route;
    }

    /**
     * Retourne la liste des parametres
     * @return array
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * Execute la route et retourne le résultat
     *
     * @return mixed
     */
    public function handle()
    {
        return Container::call($this->function, $this->params);
    }

}
