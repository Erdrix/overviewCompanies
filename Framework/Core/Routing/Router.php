<?php

class Router
{

    //////////////////////////
    // Attributs
    /////////

    /**
     * Uri par défaut
     * @var string
     */
    protected static $default;

    /**
     * Liste des routes disponnible
     * @var array
     */
    protected static $routes = [];

    /**
     * Liste des configuration
     * @var array
     */
    protected static $config = [];

    //////////////////////////
    // Static Methods
    /////////

    /**
     * Configuration des différents elements
     *
     * @param array $config : Liste des configurations
     */
    public static function config($config)
    {
        self::$config = array_merge_recursive(self::$config, $config);
    }

    /**
     * Retourne une/les configuration(s) du router
     *
     * @param  boolean $name : Nom de la configuration
     *
     * @return mixed (False si aucune valeur)
     */
    public static function getConfig($name = false)
    {
        if (false === $name) {
            return self::$config;
        } elseif (isset(self::$config[$name])) {
            return self::$config[$name];
        }

        return false;
    }

    /**
     * Retourne la route suivant la requête
     *
     * @param  Request $request : Requête
     *
     * @return Closure : Fonction contenant la route et les middlewares
     */
    public static function match($request)
    {
        $uri = false;
        if (file_exists(APP . 'routes' . DS . $request->prefix() . '.php')) {
            require APP . 'routes' . DS . $request->prefix() . '.php';
        } else {
            $uri = true;
        }

        if (!($route = self::request($request->method(), $uri ? substr($request->uri(), 0, -1) : $request->route()))) {
            self::__404();
        }

        $request->route($route->getName());

        // Préparation de la route
        $closure = function () use ($route) {
            return $route->handle();
        };

        // Préparation des middleware
        $list = self::getConfig('middleware');
        if ($list) {
            foreach (array_reverse($list) as $key) {
                $temp    = Container::get($key);
                $closure = function ($request) use ($temp, $closure) {
                    return $temp->handle($request, $closure);
                };
            }
        }

        return $closure;
    }

    /**
     * Demande la route définis en parametre
     *
     * @param  string $type : Type de requête (GET, POST, UPDDATE, DELETE)
     * @param  string $path : Url utilisé pour la redirection
     *
     * @return Route  : Route detecté par le routeur
     */
    public static function request($type, $path)
    {
        $types = [$type, '*'];
        foreach ($types as $type) {
            if (!isset(self::$routes[$type])) {
                continue;
            }

            foreach (self::$routes[$type] as $uri => $fct) {
                if (preg_match($uri, $path, $match)) {
                    $params = [];
                    foreach ($match as $key => $value) {
                        if (!is_numeric($key)) {
                            $params[$key] = $value;
                        }
                    }

                    return new Route($fct[1], $fct[0], $params, self::$config);
                }
            }
        }

        return false;
    }

    /**
     * Enregistrement d'une nouvelle route
     *
     * @param string   $type     : Type de requête (GET, POST, UPDATE, DELETE)
     * @param string   $uri      : Uri utilisé pour la redirection
     * @param Callback $callback : Fonction à utiliser lors de l'utilisation de la route
     */
    public static function respond($type, $uri, Callable $callback)
    {
        $ini = $uri;
        $uri = preg_replace('/{([a-zA-Z0-9]+)}/', '(?P<${1}>([a-zA-Z0-9\_\-\s]+))', $uri);
        $uri = '/^' . str_replace('/', '\/', $uri) . '$/';

        self::$routes[strtoupper($type)][$uri] = [$ini, $callback];
    }

    //////////////////////////
    // Private Static methods
    /////////

    /**
     * Appelle la page 404 si la route n'est pas trouvée.
     */
    private static function __404()
    {
        $event = Dispatcher::call('router.error');
        if (!$event->isCancelled()) {
            header('HTTP/1.0 404 Not Found');
            echo 'Oops, vous semblez etre perdu !';
        }

        die();
    }
}
