<?php

class Container
{

    //////////////////////////
    // Attributs
    /////////

    /**
     * Liste des classes nécessitant une nouvelle
     *  instance à chaque appel
     * @var array
     */
    private static $factories = [];

    /**
     * Liste des classes nécessitant un singleton
     * @var array
     */
    private static $registry = [];

    /**
     * Liste des singletons déja initialisés
     * @var array
     */
    private static $singletons = [];

    //////////////////////////
    // Public Methods
    /////////

    /**
     * Déclare une nouvelle classe nécessitant une nouvelle instance
     *
     * @param string   $key      : Nom de la classe
     * @param Callable $resolver : Fonction à effectuer lors de l'appel
     */
    public static function set($key, Callable $resolver)
    {
        self::$factories[strtolower($key)] = $resolver;
    }

    /**
     * Déclare une nouvelle classe nécessitant un singleton
     *
     * @param string   $key      : Nom de la classe
     * @param Callable $resolver : Fonction à effectuer lors de l'appel
     */
    public static function singleton($key, Callable $resolver)
    {
        self::$registry[strtolower($key)] = $resolver;
    }

    /**
     * Sauvegarde l'instance de classe
     *
     * @param $instance : Instance de classe
     */
    public static function instance($instance)
    {
        $reflected                                           = new ReflectionClass($instance);
        self::$singletons[strtolower($reflected->getName())] = &$instance;
    }

    /**
     * Instancie la classe à partir du nom
     *
     * @param  string $key : Nom de la classe
     *
     * @return Object
     */
    public static function get($key)
    {
        $key = strtolower($key);

        if (isset(self::$factories[$key])) {
            return call_user_func(self::$factories[$key]);
        }

        if (!isset(self::$singletons[$key])) {
            if (isset(self::$registry[$key])) {
                self::$singletons[$key] = call_user_func(self::$registry[$key]);
            } else {
                self::$singletons[$key] = self::resolve($key);
            }
        }

        return self::$singletons[$key];
    }

    /**
     * Instancie la classe à partir de son nom
     *
     * @param  $key
     *
     * @throws Exception
     * @return object
     */
    public static function resolve($key)
    {
        $key             = strtolower($key);
        $reflected_class = new ReflectionClass($key);

        if ($reflected_class->isInstantiable()) {
            if ($constructor = $reflected_class->getConstructor()) {
                $parameters             = $constructor->getParameters();
                $constructor_parameters = [];

                foreach ($parameters as $parameter) {
                    if ($parameter->getClass()) {
                        $constructor_parameters[] = self::get($parameter->getClass()->getName());
                    } else {
                        $constructor_parameters[] = $parameter->getDefaultValue();
                    }

                }

                return $reflected_class->newInstanceArgs($constructor_parameters);
            } else {
                return $reflected_class->newInstance();
            }

        } else {
            throw new Exception($key . ' is unreachable.');
        }
    }

    /**
     * Execute la closure demandé avec résolution des parametres
     *
     * @param callable $function   : Fonction à executer
     * @param array    $parametres : Parametres optionel
     */
    public static function call(Callable $function, array $parametres = [])
    {
        $reflection = new ReflectionFunction($function);
        $arguments  = $reflection->getParameters();
        $list       = [];

        foreach ($arguments as $parametre) {
            if ($parametre->getClass()) {
                if (isset($parametres[$parametre->getClass()->getName()])) {
                    $list[] = $parametres[$parametre->getClass()->getName()];
                } else {
                    $list[] = self::get($parametre->getClass()->getName());
                }
            } else {
                if (isset($parametres[$parametre->getName()])) {
                    $list[] = $parametres[$parametre->getName()];
                } else {
                    $list[] = $parametre->getDefaultValue();
                }
            }

        }

        return $reflection->invokeArgs($list);
    }
}
