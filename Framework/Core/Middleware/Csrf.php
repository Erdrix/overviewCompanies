<?php

class Csrf implements Middleware
{

    //////////////////////////
    // Attributs
    /////////

    /**
     * Authentification
     * @var Auth
     */
    private $auth;

    //////////////////////////
    // Constructeur
    /////////

    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    //////////////////////////
    // Handler
    /////////

    /**
     * Gestion de la requête
     *
     * @param Request $request : Informations sur la requête
     * @param Closure $next    : Prochaine closure
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($this->auth->logged()) {
            if (!empty($_POST)) {
                if (!CSRFToken::check()) {
                    return Redirect::make('');
                }
            }
        }

        return $next($request);
    }
}