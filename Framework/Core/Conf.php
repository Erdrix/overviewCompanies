<?php

class Conf
{

    //////////////////////////
    // Attributs
    /////////

    /**
     * Liste des configurations
     * @var array
     */
    private static $data = [];

    //////////////////////////
    // Static Methods
    /////////

    /**
     * Ajoute une liste de configuration
     *
     * @param array $array : Liste de configuration
     */
    public static function push($array)
    {
        if (is_array($array)) {
            self::$data = array_merge(self::$data, $array);
        }

    }

    /**
     * Récupération d'une configuration
     *
     * @param  string $name : Nom de la configuration
     *
     * @return mixed  : False si aucun élement dans la liste
     */
    public static function get($name)
    {
        $name = explode('.', $name);
        $data = self::$data;

        foreach ($name as $part) {
            if (!isset($data[$part])) {
                return false;
            }

            $data = $data[$part];
        }

        return $data;
    }

    /**
     * Retourne toute la liste des configurations
     * @return array
     */
    public static function getAll()
    {
        return self::$data;
    }

}
