<?php

class Request
{

    //////////////////////////
    // Attributs
    /////////

    /**
     * Uri demandé par l'utilisateur
     * @var string
     */
    private $uri;

    /**
     * Route demandée par l'utilisateur
     * @var string
     */
    private $route;

    /**
     * Prefix de la route
     * @var string
     */
    private $prefix;

    /**
     * Methode utilisée lors de la requête
     * @var string
     */
    private $method;

    /**
     * Action utilisée lors de la requête
     * @var string
     */
    private $action;

    /**
     * Variable serveur
     * @var array
     */
    private $server;

    /**
     * Variable de session
     * @var array
     */
    private $session;

    //////////////////////////
    // Constructeur
    /////////

    public function __construct()
    {
        $conf  = Conf::get('global');
        $route = isset($_GET['route']) ? (!empty($_GET['route']) ? $_GET['route'] : $conf['default']['route']) : $conf['default']['route'];
        $url   = explode('/', trim($route, '/'));

        $this->server = $_SERVER;
        $this->method = ($_SERVER['REQUEST_METHOD'] == 'POST' && array_key_exists('HTTP_X_HTTP_METHOD',
                $_SERVER)) ? $_SERVER['HTTP_X_HTTP_METHOD'] : $_SERVER['REQUEST_METHOD'];
        $this->prefix = isset($url[0]) ? array_shift($url) : '';
        $this->action = implode('/', $url);;
        $this->route = '/' . $this->action;
        $this->uri   = '/' . $route;
    }

    //////////////////////////
    // Public Methods
    /////////

    /**
     * Retourne l'ip de l'utilisateur
     *
     * @return string
     */
    public function ip()
    {
        return isset($this->server['HTTP_X_FORWARDED_FOR'])
            ? $this->server['HTTP_X_FORWARDED_FOR'] : $this->server['REMOTE_ADDR'];
    }

    /**
     * Retourne si la requête est envoyé par un script ajax
     *
     * @return boolean
     */
    public function ajax()
    {
        return (!empty($this->server['HTTP_X_REQUESTED_WITH']) &&
                strtolower($this->server['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest');
    }

    /**
     * Retourne si la connexion est sécurisé
     *
     * @return boolean
     */
    public function secure()
    {
        return isset($_SERVER['HTTPS']);
    }

    /**
     * Retourne la méthode utilisée lors de la requête
     * @return string
     */
    public function method()
    {
        return $this->method;
    }

    /**
     * User-agent utilisé lors de la requête
     *
     * @param string $user_agent
     *
     * @return null
     */
    public function user_agent($user_agent = null)
    {
        if (null !== $user_agent) {
            $this->server['HTTP_USER_AGENT'] = $user_agent;
        }

        return $this->server['HTTP_USER_AGENT'];
    }

    /**
     * Retourne l'uri demandé par l'utilisateur
     *
     * @param string $uri
     *
     * @return string
     */
    public function uri($uri = null)
    {
        if (null !== $uri) {
            $this->uri = $uri;
        }

        return $this->uri;
    }

    /**
     * Retourne la route demandé par l'utilisateur
     *
     * @param string $route
     *
     * @return string
     */
    public function route($route = null)
    {
        if (null !== $route) {
            $this->route = $route;
        }

        return $this->route;
    }

    /**
     * Retourne le prefix de la route
     *
     * @param string $prefix
     *
     * @return string
     */
    public function prefix($prefix = null)
    {
        if (null !== $prefix) {
            $this->prefix = $prefix;
        }

        return $this->prefix;
    }

    /**
     * Retourne l'action utilisée lors de la requête
     *
     * @param string $action
     *
     * @return string
     */
    public function action($action = null)
    {
        if (null !== $action) {
            $this->action = $action;
        }

        return $this->action;
    }

    /**
     * Définis ou retourne une clée serveur
     *
     * @param bool $key   : Nom de la clée
     * @param null $value : Valeur de la clée si renseigné
     *
     * @return mixed False si aucune valeur
     */
    public function server($key = false, $value = null)
    {
        if (null === $value) {
            if (false === $key) {
                return $this->server;
            }

            return isset($this->server[$key]) ? $this->server[$key] : false;
        } else {
            $this->server[$key] = $value;

            return true;
        }
    }
}