<?php

class Auth
{

    //////////////////////////
    // Attributs
    /////////

    /**
     * Base de donnée
     * @var Database
     */
    private $database;

    /**
     * Nom de la table
     * @var string
     */
    private $table;

    /**
     * Utilisateur
     * @var User
     */
    private $user;

    //////////////////////////
    // Constructeur
    /////////

    public function __construct(Database $database)
    {
        $this->database = $database;
        $this->table    = Conf::get('global.table_user');

        if ($this->logged()) {
            $this->user = $_SESSION['auth'];
        } else {
            $this->user = new User();
        }
    }

    //////////////////////////
    // Public Methods
    /////////

    /**
     * Connexion d'un utilisateur
     *
     * @param string $username : Nom d'utilisateur
     * @param string $password : Mot de passe utilisateur
     *
     * @return boolean : False si impossible de connecter
     */
    public function login($username, $password)
    {
        if (!$this->table || $this->logged()) {
            return false;
        }

        $user = User::finds(['username' => strtolower($username)], '*', 1);
        if (!$user) {
            return false;
        }

        if (!Hash::check($password, $user->password)) {
            return false;
        }

        Session::regenerate();
        unset($user->password);
        $_SESSION['auth'] = $user;
        $this->user       = $user;

        return true;
    }

    /**
     * Déconnexion d'un utilisateur
     *
     * @return boolean False si erreur lors de la déconnexion
     */
    public function logout()
    {
        if (!$this->table || !$this->logged()) {
            return false;
        }


        Session::destroy();
        $this->user = new User();

        return true;
    }

    /**
     * Crée un nouveau compte dans la base de donnée
     *
     * @param string $username : Nom du compte
     * @param string $password : Mot de passe du compte
     *
     * @return boolean False si impossible de créer le compte
     */
    public function register($username, $password)
    {
        if (!$this->table) {
            return false;
        }

        $user = User::finds(['username' => strtolower($username)], ['id'], 1);
        if ($user) {
            return false;
        }

        $user           = new User();
        $user->username = $username;
        $user->password = Hash::create($password);

        return $user->save() ? $user : false;
    }

    /**
     * Retourne l'utilisateur en cours
     * @return User
     */
    public function user()
    {
        return $this->user;
    }

    /**
     * Retourne si l'utilisateur est déja connecté
     *
     * @return mixed
     */
    public function logged()
    {
        return isset($_SESSION['auth']);
    }

    /**
     * Retourne si l'utilisateur connecté à les permissions
     *
     * @param string $permission
     *
     * @return boolean
     */
    public function has($permission)
    {
        return $this->user()->has($permission);
    }
}