<?php

class User extends Model
{

    /**
     * Nom de la table
     * @var string
     */
    protected static $_table = 'users_profils';

    protected static $_join = [
        'fields'      => ['idField', 'id'],
        'users_roles' => ['idRole', 'id']
    ];

    //////////////////////////
    // Attributs
    /////////

    /**
     * Nom d'utilisateur
     * @var string
     */
    public $username = 'Guest';

    /**
     * Permission de l'utilisateur
     * @var array
     */
    public $_permissions;

    //////////////////////////
    // Public Methods
    /////////

    /**
     * Retourne si l'utilisateur est connecté
     * @return boolean
     */
    public function logged()
    {
        return $this->username !== 'Guest';
    }

    /**
     * Retourne si l'utilisateur à la permissions demandé
     *
     * @param string $permission
     *
     * @return boolean
     */
    public function has($permission)
    {
        $list = $this->_permissions();
        if ($list === false) {
            return false;
        }

        $has = false;
        if (isset($list[$permission])) {
            $has = $list[$permission];
        } else {
            if (isset($list['*']) && is_numeric($list['*'])) {
                $has = $list['*'];
            } else {
                if (isset($list['*.*'])) {
                    $has = $list['*.*'];
                }

                $permission = explode('.', $permission);
                $perm       = '';
                $first      = true;
                foreach ($permission as $key) {
                    $perm .= ($first ? '' : '.') . $key;
                    if (isset($list['*' . $perm])) {
                        $has = $list['*.' . $perm];
                    } elseif (isset($list['*.' . $key])) {
                        $has = $list['*.' . $key];
                    }
                }
            }
        }

        return !empty($has);
    }

    //////////////////////////
    // Private Methods
    /////////

    /**
     * Retourne la liste des permissions
     *
     * @return array|boolean False si aucune permission
     */
    private function _permissions()
    {
        if (isset($this->_permissions)) {
            return $this->_permissions;
        }

        if (isset($this->users_roles_permissions)) {
            $perm               = flattenAssoc(json_decode($this->users_roles_permissions, true));
            $this->_permissions = $perm;

            return $this->_permissions;
        }

        return false;
    }

}