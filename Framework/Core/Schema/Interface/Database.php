<?php

Interface Database
{

    //////////////////////////
    // Public Methods
    /////////

    /**
     * Définis une class comme valeur de retour
     *
     * @param string $class : Nom de la classe
     *
     * @return Database
     */
    public function setFetch($class);


    /**
     * Définis les rêgles de jointure
     *
     * @param array $join : Liste des jointures
     *
     * @return Database
     */
    public function setJoin(array $join);

    /**
     * Retourne la structure de la table
     *
     * @param string $table : Nom de la table
     *
     * @return mixed
     */
    public function describe($table);

    /**
     * Recherche une/des entrée(s) suivant les le/les critère(s)
     *
     * @param  string $table    : Nom de la table à utiliser
     * @param  array  $criteres : Critères de séléction Array['critere' => 'valeur'(, 'critere' => 'valeur')]
     * @param  string $data     : Elements à retourner Array['Element'(, 'Element')] (default *)
     * @param  mixed  $limit    : Limite d'elements à retourner (default 50)
     * @param  mixed  $order    : Ordre des elements à retourner (default false)
     *
     * @return mixed Résultat de la requête sql
     */
    public function search($table, $criteres = [], $data = '*', $limit = 50, $order = false);

    /**
     * Execution d'une requête sql par PDO::prepare
     *
     * @param  string  $query : Requête sql
     * @param  array   $data  : Données d'execution de la requête
     * @param  boolean $fetch : Execute fetchAll (default false
     * @param  boolean $one   : Retourne un element de la base
     *
     * @return mixed Résultat de la requête si fetch à true
     */
    public function query($query, $data = [], $fetch = false, $one = false);

    /**
     * Transforme un tableau en critères de recherche SQL
     *
     * @param  array $criteres : Array['critere' => 'valeur'(, 'critere' => 'valeur')]
     *
     * @return string Critères de recherche de requête SQL 'WHERE'
     */
    public function parseSQLWhere($criteres);

    /**
     * Efface une/des entrée(s) suivant les le/les critère(s)
     *
     * @param  string $table    : Nom de la table à utiliser
     * @param  array  $criteres : Array['critere' => 'valeur'(, 'critere' => 'valeur')]
     *
     * @return boolean False si une erreur est survenue
     */
    public function delete($table, $criteres);

    /**
     * Insert dans la table la/les valeur(s) contenu dans le tableau
     *
     * @param  string $table : Nom de la table à utiliser
     * @param  array  $data  : Array['critere' => 'valeur'(, 'critere' => 'valeur')]
     *
     * @return boolean False si une erreur est survenue
     */
    public function insert($table, array $data);

    /**
     * Modifie la/les donnée(s) suivant le/les critère(s)
     *
     * @param  string $table    : Nom de la table à utiliser
     * @param  array  $data     : Array['critere' => 'valeur'(, 'critere' => 'valeur')]
     * @param  array  $criteres : Array['critere' => 'valeur'(, 'critere' => 'valeur')]
     *
     * @return boolean False si une erreur est survenue
     */
    public function update($table, $data, $criteres);

    /**
     * Recherche si le/les critère(s) sont présent dans la base de données
     *
     * @param  string $table    : Nom de la table à utiliser
     * @param  array  $criteres : Array['critere' => 'valeur'(, 'critere' => 'valeur')]
     *
     * @return boolean True si présent dans la base de données
     */
    public function isexist($table, $criteres);

}