<?php

interface Response
{

    /**
     * Execute la réponse
     */
    public function call();
}
