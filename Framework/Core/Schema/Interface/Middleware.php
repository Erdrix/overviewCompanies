<?php

interface Middleware
{

    /**
     * Gestion de la requête
     *
     * @param Request $request : Informations sur la requête
     * @param Closure $next    : Prochaine closure
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next);
}