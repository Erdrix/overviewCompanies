<?php

abstract class Model implements ArrayAccess
{

    //////////////////////////
    // Attributs
    /////////

    /**
     * Table utilisé lors des requêtes
     * @var string
     */
    protected static $_table;
    /**
     * Champ d'identification de la table
     * @var string
     */
    protected static $_id = 'id';

    /**
     * Variable en blacklist
     * Impossible à inserer
     * @var array
     */
    protected static $_blacklist = [];

    /**
     * Jointure
     * @var array
     */
    protected static $_join = [];

    /**
     * Base de donnée
     * @var Object
     */
    private static $database = null;

    //////////////////////////
    // Static Methods
    /////////

    /**
     * Sauvegarde un nouvel enregistrement
     *
     * @param array $data : Valeur de l'enregistrement
     *
     * @return boolean | integer L'id de l'enregistrement ou false si erreur
     */
    public static function create(array $data)
    {
        return self::getDatabase()->insert(static::$_table, $data);
    }

    /**
     * Modifie un enregistrement existant
     *
     * @param integer $id   : Id de l'enregistrement
     * @param array   $data : Nouvelle valeur de l'enregistrement
     *
     * @return boolean | integer L'id de l'enregistrement ou false si erreur
     */
    public static function update($id, array $data)
    {
        return self::getDatabase()->update(static::$_table, $data, [static::$_id => $id]);
    }

    /**
     * Supprime un enregistrement
     *
     * @param integer $id : Id de l'enregistrement
     *
     * @return boolean
     */
    public static function delete($id)
    {
        return self::getDatabase()->delete(static::$_table, [static::$_id => $id]);
    }

    /**
     * Retourne un element de la base de donnée suivant l'id
     *
     * @param $id : Id de l'element
     *
     * @return Model
     */
    public static function find($id)
    {
        return static::finds([static::$_id => $id], '*', 1);
    }

    /**
     * Retourne une liste d'elements identifié par leurs id
     *
     * @param array $elements : Element à récupérer
     *
     * @return array
     */
    public static function identifiants(array $elements = [])
    {
        $id   = static::$_id;
        $data = static::finds([], array_merge([static::$_id], $elements));
        $temp = [];
        foreach ($data as $value) {
            $temp[$value->$id] = $value;
        }

        return $temp;
    }

    /**
     * Retourne plusieurs elements suivant une liste de criteres
     *
     * @param array            $critere : Liste de criteres
     * @param string | array   $data    : Liste des elements à récupérer
     * @param integer          $limit   : Limite d'élements à récupérer
     * @param boolean | string $order   : Ordre de récupération (false pour désactivé)
     *
     * @return mixed
     */
    public static function finds(array $critere, $data = '*', $limit = 50, $order = false)
    {
        return static::getDatabase()->setFetch(get_called_class())->setJoin(static::$_join)->search(static::$_table,
            $critere, $data, $limit, $order);
    }

    /**
     * Execute une requête dans la base de donnée
     *
     * @param string     $query      : Requête à executer
     * @param null|array $attributes : Elements à passer à la requête
     * @param boolean    $one        : Retourner un unique element
     *
     * @return mixed
     */
    public static function query(
        $query,
        $attributes = null,
        $one = false
    ) {
        return static::getDatabase()->setFetch(get_called_class())->query($query, $attributes, true, $one);
    }

    /**
     * Retourne tout les elements de la table dans la limite indiquée
     *
     * @param boolean | integer | string $limit : Limite d'elements à récuperer
     * @param boolean | string           $order : Ordre de récupération des elements
     *
     * @return mixed
     */
    public static function all($limit = false, $order = false)
    {
        return static::getDatabase()->setFetch(get_called_class())->setJoin(static::$_join)->search(static::$_table, [],
            '*', $limit, $order);
    }

    /**
     * Retourne la base de donnée enregistré
     *
     * @return Database
     */
    protected static function getDatabase()
    {
        if (self::$database === null) {
            self::$database = Container::get('Database');
        }

        return self::$database;
    }

    //////////////////////////
    // Public Methods
    /////////

    /**
     * Getter magique en cas de variable introuvable
     *
     * @param string $key : Nom de la variable demandée
     *
     * @return mixed
     */
    public function __get($key)
    {
        $method = 'get' . ucfirst($key);

        try {
            $this->$key = $this->$method();
        } catch (Exception $e) {
            return null;
        }

        return $this->$key;
    }

    /**
     * Sauvegarde l'instance présent et ajoute l'id à l'objet
     *
     * @return False si impossible de sauvegarder l'instance
     */
    public function save()
    {
        $key = static::$_id;

        if (empty($this->$key)) {
            $id = static::create(get_object_vars($this));
            if ($id === false) {
                return false;
            }

            $this->$key = $id;
        } else {
            $data = get_object_vars($this);
            if (static::update($data->$key, $data) === false) {
                return false;
            }
        }

        return true;
    }

    //////////////////////////
    // Array Access
    /////////

    /**
     * Définis une clée
     *
     * @param mixed $offset : Nom de la clée
     * @param mixed $value  : Valeur de la clée
     */
    public function offsetSet($offset, $value)
    {
        if (!is_null($offset)) {
            $this->$offset = $value;
        }
    }

    /**
     * Retourne si une clée existe
     *
     * @param mixed $offset : Nom de la clée
     *
     * @return bool
     */
    public function offsetExists($offset)
    {
        return isset($this->$offset);
    }

    /**
     * Suppression d'une clée
     *
     * @param mixed $offset : Nom de la clée
     */
    public function offsetUnset($offset)
    {
        unset($this->$offset);
    }

    /**
     * Récupération d'une clée
     *
     * @param mixed $offset : Nom de la clée
     *
     * @return mixed|null
     */
    public function offsetGet($offset)
    {
        return isset($this->$offset) ? $this->$offset : null;
    }

}