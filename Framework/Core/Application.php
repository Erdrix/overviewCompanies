<?php

class Application
{

    //////////////////////////
    // Attributes
    /////////

    /**
     * Requête généré par l'application
     * @var Request
     */
    private static $request;

    //////////////////////////
    // Constructeur
    /////////

    /**
     * Création de l'application
     */
    public function __construct()
    {
        // -- Lancement du Benchmarking
        if (Conf::get('global.benchmark')) {
            Benchmark::start(TIME);
        }

        // -- Protection des variables global
        $this->removeMagicQuotes();
        $this->unregisterGlobals();

        // -- Création de la requête
        self::$request = new Request();
        Container::instance(self::$request);

        // -- Importation des modules
        Benchmark::mark('modules');
        if ($modules = Conf::get('modules')) {
            foreach ($modules as $module) {
                $module::register();
            }
        }
        Router::config(['middleware' => Conf::get('middlewares')]);
        Benchmark::end('modules');

        // -- Routing
        Benchmark::mark('routing');
        $closure = Router::match(self::$request);
        Benchmark::end('routing');

        // -- Request
        Benchmark::mark('request');
        $data = $closure(self::$request);
        if ($data instanceof Response) {
            $data->call();
        } else {
            extract(Router::getConfig('variables'));
            echo $data;
        }
        Benchmark::end('request');

        // -- Efface les anciennes session flash
        Flash::endSession();

        // -- Save Benchmarking
        Benchmark::save();
    }

    //////////////////////////
    // Static Methods
    /////////

    /**
     * Charge la vue ciblé
     *
     * @param string $vue    : Chemin vers la vue
     * @param array  $params : Variables du rendu
     */
    public static function render($vue, $params = [])
    {
        $view = new View($vue, $params, false);
        $view->call();
    }

    /**
     * Appelle d'un chemin du router
     *
     * @param string $type : Type de requête (GET, POST, ...)
     * @param string $path : Chemin de la requête
     */
    public static function request($type, $path)
    {
        // Routing
        Benchmark::mark('routing');
        $closure = Router::request($type, $path);
        Benchmark::end('routing');

        // Request
        if ($closure) {
            Benchmark::mark('request');
            $data = $closure->handle();
            if ($data instanceof Response) {
                $data->call();
            } elseif ($data) {
                extract(Router::getConfig('variables'));
                echo $data;
            }
            Benchmark::end('request');
        }
    }

    /**
     * Retourne l'objet requête généré
     * @return Request
     */
    public static function getRequest()
    {
        return self::$request;
    }

    //////////////////////////
    // Private methods
    /////////

    /**
     * Protection des variables contre différentes attaques
     */
    private function removeMagicQuotes()
    {
        if (get_magic_quotes_gpc()) {
            $_GET    = strip_slashes_deep($_GET);
            $_POST   = strip_slashes_deep($_POST);
            $_COOKIE = strip_slashes_deep($_COOKIE);
        }
    }

    /**
     * Suppression des variables définis comme global
     */
    private function unregisterGlobals()
    {
        if (ini_get('register_globals')) {
            $array = ['_SESSION', '_POST', '_GET', '_COOKIE', '_REQUEST', '_SERVER', '_ENV', '_FILES'];
            foreach ($array as $value) {
                foreach ($GLOBALS[$value] as $key => $var) {
                    if ($var === $GLOBALS[$key]) {
                        unset($GLOBALS[$key]);
                    }

                }
            }
        }
    }
}
