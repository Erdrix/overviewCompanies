<?php

/**
 * Framework légé inspiré de Laravel et basé sur le pattern MVC
 *  Contenant une gestion des routes, des requêtes, ...
 *
 * @author Mouchel Thomas <thomspirit@gmail.com>
 */

/**
 * Création des variables global
 *  DS   : Séparateur utilisé lors d'utilisation de fichiers
 *  CORE : Dossier coeur du framework
 *  APP  : Dossier contenant l'application utilisateur
 *  URL  : Dossier d'hebergement du framework
 *  TIME : Timestamp lors du lancement de la page
 */
define('DS', DIRECTORY_SEPARATOR);
define('ROOT', dirname(dirname(__FILE__)));
define('CORE', ROOT . DS . 'Framework' . DS);
define('APP', ROOT . DS . 'Application' . DS);
define('URL', dirname(dirname($_SERVER['SCRIPT_NAME'])));
define('TIME', microtime(true));

/**
 * Chargement du script principal
 */
require CORE . 'Bootstrap' . DS . 'Bootstrap.php';
