window.exports = {
    target: function () {
        $('.prettyprint').each(function () {
            var target = $(this).attr('id');
            var number = false;

            $(this).find("li").each(function () {
                if (number == false) {
                    number = $(this).attr('value');
                } else if (number == target) {
                    $(this).attr('class', 'target');
                    return false;
                }
                number++;
            });
        });
    }
}

$(document).ready(function () {
    $(document).on('keydown', function (e) {
        if (e.ctrlKey) {
            if (e.which == 38) {
                $('.menu_box.active').prev().click();
                e.preventDefault();
            } else if (e.which == 40) {
                $('.menu_box.active').next().click();
                e.preventDefault();
            }
        }
    });

    $('.menu_box').click(function () {
        $('.menu_box.active').removeClass('active');
        $(this).addClass('active');
        $('.data.current').removeClass('current');
        $('.data.box-' + $(this).attr('id')).addClass('current');
    });
});