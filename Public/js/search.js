$(document).ready(function() {

	/**
	 * Return if string is empty
	 */
	String.prototype.isEmpty = function() {
	    return (this.length === 0 || !this.trim());
	};

	/**
	 * On search input validate
	 */
	$('#search_value').keypress(function(e)
	{
		if(e.which == 13)
			jQuery('#search').click();
	});

	/**
	 * On search button click
	 */
    $('#search').click(function()
    {
    	var url = $(this).attr('req');
		if(url != undefined && !url.isEmpty())
		{
	    	var data  = ($("#search_value").val() != "") ? $("#search_value").val() : "*";
            var token = $("#CSRFToken").val();
	        $.ajax({
	            type: 'POST',
	            url: url,
	            data: {data: data, CSRFToken: token},
	            cache: false,
				success: function (result) {
					var table = $('#table_body');
					table.html('');
					table.append(result);
				},
				error: function(data, errorThrown)
				{
					console.log('Request failed : ' + errorThrown);
				}
	        });
	    }
    });

	/**
	 * On Advanced Button click
	 */
	$('#advanced_button').click(function(e)
    {
    	e.preventDefault();
    	var elem = $('#advanced_search');
    	if(elem.is(':hidden'))
    		elem.fadeIn();
    	else
    		elem.fadeOut();
    });

	/**
	 * On Advanced Search validate
	 */
    $('#advanced_search').keypress(function(e)
	{
		if(e.which == 13)
		{
			var request = '';
			var inputs  = $('#advanced_search').find('input');

			var tag;
			$('#advanced_search').find('input[type=text]').each(function(){
				if(!$(this).val().isEmpty())
				{
					tag = $(this).attr('search');
					if(tag != undefined && !tag.isEmpty())
						request += '@' + tag + ':' + $(this).val() + ' ';
					else
						request += $(this).val() + ' ';
				}
			});

			$("#search_value").val(request.trim());
			$('#search').click();
		}
	});
});