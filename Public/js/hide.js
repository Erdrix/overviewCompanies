$(document).ready(function () {
    // Changement sur le select personne
    $('#stage\\[idStudent\\]').change(function () {
        hide_people(this);
    });

    // Changement sur le select entreprise
    $('#stage\\[idCompany\\]').change(function () {
        hide_company(this);
    });

    // Changement sur le select entreprise
    $('#taxe\\[idCompany\\]').change(function () {
        hide_company(this);
    });

    // Création d'un étudiant
    function hide_people(elem) {
        if ($(elem).val() == -1) {
            $('#hidden_people').fadeIn();
        } else {
            $('#hidden_people').fadeOut();
        }
    }

    // Création d'une entreprise
    function hide_company(elem) {
        if ($(elem).val() == -1) {
            $('#hidden_company').fadeIn();
        } else {
            $('#hidden_company').fadeOut();
        }
    }

    // Lancement au chargement de la page
    hide_people("#stage\\[idStudent\\]");
    hide_company("#stage\\[idCompany\\]");
    hide_company("#taxe\\[idCompany\\]");
});