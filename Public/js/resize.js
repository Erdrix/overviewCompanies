$(document).ready(function() {
    function resize() {
        $('.tuile_50').width($(window).width() / 2 - 40);
        $('.tuile_25').width(($(window).width() / 2 - 62) / 2);
    }

    $(window).resize(function(){
        resize();
    });

    resize();
});