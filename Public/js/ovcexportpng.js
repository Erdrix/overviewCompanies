(function($){
	 $.fn.ovcExportPNG = function(callback) {

	 	// Récupère l'élément à exporter en png.
        var el = $(this);
        html2canvas(el, {

            onrendered: function(canvas) {
                ctx = canvas.getContext('2d');
                // Render connections on top of same canvas
                svg= $("svg", el);	
				svg.each(function() {
                    $svg = $(this)
                    svgStr = this.outerHTML;
                    ctx.drawSvg(svgStr, $svg.css("left"), $svg.css("top"));
                });
                var img = Canvas2Image.convertToPNG(canvas); // transforme en <img src="data:image/png;base64,..."
                callback(img);
            }
        });
    };
})(jQuery)